/**
  ******************************************************************************
  * @file    PWR/PWR_CurrentConsumption/stm32f0xx_lp_modes.c 
  * @author  MCD Application Team
  * @version V1.4.0
  * @date    24-July-2014
  * @brief   This file provides firmware functions to manage the following 
  *          functionalities of the STM32F0xx Low Power Modes:
  *           - Sleep Mode
  *           - STOP mode with RTC
  *           - STANDBY mode without RTC
  *           - STANDBY mode with RTC
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_lp_modes.h"
#include "stm32f0xx.h"
#include "led.h"
#include "buzzer.h"
#include "usb_bsp.h"
#include "usbd_cdc_vcp.h"
#include "usbd_core.h"
#include "usbd_conf.h"
/* Includes ------------------------------------------------------------------*/
#include "usbd_cdc_vcp.h"
#include "usbd_cdc_core.h"

/** @addtogroup STM32F0xx_StdPeriph_Examples
  * @{
  */

/** @addtogroup PWR_CurrentConsumption
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

volatile uint8_t stop_mode_exit = 0;

extern volatile uint8_t vsense_result;

extern USB_CORE_HANDLE USB_Device_dev;
USB_CORE_HANDLE *dev_mode = &USB_Device_dev;
extern CDC_IF_Prop_TypeDef VCP_fops;
/*
= 
{
  VCP_Init,
  VCP_DeInit,
  VCP_Ctrl,
  VCP_DataTx,
  VCP_DataRx
};
*/

/**
  * @brief  Configures system clock after wake-up from STOP: enable HSE, PLL
  *         and select PLL as system clock source.
  * @param  None
  * @retval None
  */
void SYSCLKConfig_STOP(void)
{  
  /* After wake-up from STOP reconfigure the system clock */
  /* Enable HSE */
	uint32_t loop;
	
	//RCC_HSEConfig(RCC_HSE_OFF);
  RCC_HSEConfig(RCC_HSE_ON);
  
  /* Wait till HSE is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET)
//	for(loop == 0; loop < 5000000; loop++);
  {}
  
  /* Enable PLL */
  RCC_PLLCmd(ENABLE);
  
  /* Wait till PLL is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
//for(loop == 0; loop < 5000000; loop++);  
{}
  
  /* Select PLL as system clock source */
  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
  
  /* Wait till PLL is used as system clock source */
 while (RCC_GetSYSCLKSource() != 0x08)
	//for(loop == 0; loop < 5000000; loop++);
  {}
}


static void SetClock(void)
{
  __IO uint32_t StartUpCounter = 0, HSEStatus = 0;
  
  /* SYSCLK, HCLK, PCLK configuration ----------------------------------------*/
  /* Enable HSE */    
  RCC->CR |= ((uint32_t)RCC_CR_HSEON);
 
  /* Wait till HSE is ready and if Time out is reached exit */
  do
  {
    HSEStatus = RCC->CR & RCC_CR_HSERDY;
    StartUpCounter++;  
  } while((HSEStatus == 0) && (StartUpCounter != HSE_STARTUP_TIMEOUT));

  if ((RCC->CR & RCC_CR_HSERDY) != RESET)
  {
    HSEStatus = (uint32_t)0x01;
  }
  else
  {
    HSEStatus = (uint32_t)0x00;
  }  

  if (HSEStatus == (uint32_t)0x01)
  {
    /* Enable Prefetch Buffer and set Flash Latency */
    FLASH->ACR = FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY;
 
    /* HCLK = SYSCLK */
    RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV1;
      
    /* PCLK = HCLK */
    RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE_DIV1;

    /* PLL configuration = HSE * 6 = 48 MHz */
    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_PLLSRC | RCC_CFGR_PLLXTPRE | RCC_CFGR_PLLMULL));
    RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC_PREDIV1 | RCC_CFGR_PLLXTPRE_PREDIV1 | RCC_CFGR_PLLMULL6);
            
    /* Enable PLL */
    RCC->CR |= RCC_CR_PLLON;

    /* Wait till PLL is ready */
    while((RCC->CR & RCC_CR_PLLRDY) == 0)
    {
    }

    /* Select PLL as system clock source */
    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
    RCC->CFGR |= (uint32_t)RCC_CFGR_SW_PLL;    

    /* Wait till PLL is used as system clock source */
    while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS) != (uint32_t)RCC_CFGR_SWS_PLL)
    {
    }
  }
  else
  { /* If HSE fails to start-up, the application will have wrong clock 
         configuration. User can add here some code to deal with this error */
  }  
}



void EXTI4_15_IRQHandler(void) {
	
	EXTI_InitTypeDef  EXTI_InitStructure;

//	static uint8_t power_count = 0;
	

//	if(stop_mode_exit == 1) {
		if(EXTI_GetITStatus(EXTI_Line13) != RESET) {
		
			stop_mode_exit = 1;
	//		SYSCLKConfig_STOP();
			
	//		 NVIC_SystemReset();



			
//			Init_Target ();			   //Initialize all hardware peripherals
			
//			/* Clear the EXTI line 13 pending bit */
//			 EXTI_ClearITPendingBit(EXTI_Line13);
				
//			 EXTI_InitStructure.EXTI_Line = EXTI_Line13;
//			 EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//			 EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
//			 EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//			 EXTI_Init(&EXTI_InitStructure);
		}
		
		
//		stop_mode_exit =0;
//	}
	
//	else if(stop_mode_exit == 0) {
//		vsense_result = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13);
//		if(!vsense_result) {
//			StopMode_Measure();   							//call low power modes used for rtc	
//		}
//	}
	
	/* Clear the EXTI line 13 pending bit */
		EXTI_ClearITPendingBit(EXTI_Line13);
		
		
}

//void EXTI4_15_IRQHandler(void) {
//	
//	EXTI_InitTypeDef  EXTI_InitStructure;

////	if(stop_mode_exit == 1) {
//		if(EXTI_GetITStatus(EXTI_Line13) != RESET) {
//		
//			SYSCLKConfig_STOP();
//			usbd_init();					 //Initialize USB in device mode
//			Init_Target ();			   //Initialize all hardware peripherals
//			
///*			/* Clear the EXTI line 13 pending bit */
////			 EXTI_ClearITPendingBit(EXTI_Line13);
//				
////			 EXTI_InitStructure.EXTI_Line = EXTI_Line13;
////			 EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
////			 EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
////			 EXTI_InitStructure.EXTI_LineCmd = ENABLE;
////			 EXTI_Init(&EXTI_InitStructure);
//		}
////		stop_mode_exit =0;
////	}
//	
////	else if(stop_mode_exit == 0) {
////		vsense_result = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13);
////		if(!vsense_result) {
////			StopMode_Measure();   							//call low power modes used for rtc	
////		}
////	}
//	
//	/* Clear the EXTI line 13 pending bit */
//		EXTI_ClearITPendingBit(EXTI_Line13);
//		
//		
//}
	


/**
  * @brief  This function configures the system to enter Sleep mode for
  *         current consumption measurement purpose.
  *         Sleep Mode
  *         ==========  
  *            - System Running at PLL (48MHz)
  *            - Flash 1 wait state
  *            - Prefetch and Cache enabled
  *            - Code running from Internal FLASH
  *            - All peripherals disabled.
  *            - Wakeup using EXTI Line (KEY Button PB.08)
  * @param  None
  * @retval None
  */
void SleepMode_Measure(void)
{
  __IO uint32_t index = 0;
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Configure all GPIO as analog to reduce current consumption on non used IOs */
  /* Enable GPIOs clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC |
                        RCC_AHBPeriph_GPIOD | RCC_AHBPeriph_GPIOF , ENABLE);
  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIO_Init(GPIOF, &GPIO_InitStructure);
  GPIO_Init(GPIOA, &GPIO_InitStructure); 
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* Disable GPIOs clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC |
                         RCC_AHBPeriph_GPIOD | RCC_AHBPeriph_GPIOF, DISABLE);


  /* Request to enter SLEEP mode */
  __WFI();
	
	LED_OAMP();

  /* Infinite loop */
  while (1)
  {
		TURN_OAMP_ON;//turn on led

    /* Inserted Delay */
    for(index = 0; index < 0x7FFFF; index++);
  }
}

/**
  * @brief  This function configures the system to enter Stop mode with RTC 
  *         clocked by LSI for current consumption measurement purpose.
  *         STOP Mode with RTC clocked by LSI
  *         =====================================   
  *           - RTC Clocked by LSI
  *           - Regulator in LP mode
  *           - HSI, HSE OFF and LSI OFF if not used as RTC Clock source
  *           - No IWDG
  *           - FLASH in deep power down mode
  *           - Automatic Wakeup using RTC clocked by LSI (~5s)
  * @param  None
  * @retval None
  */
////void StopMode_Measure(void)
////{
////  __IO uint32_t index = 0;
////  GPIO_InitTypeDef GPIO_InitStructure;
////  NVIC_InitTypeDef  NVIC_InitStructure;
////  EXTI_InitTypeDef  EXTI_InitStructure;
////	

////	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);	
////	/* Configure EXTI Line13 */
////  EXTI_InitStructure.EXTI_Line = EXTI_Line13;
////  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
////  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
////  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
////  EXTI_Init(&EXTI_InitStructure);
////	
////	
//////		LED_OAMP();
//////		TURN_OAMP_ON;      //turn on led
//////		for(index = 0; index < 0x1FFFFFFF; index++);
//////		for(index = 0; index < 0x1FFFFFFF; index++);
//////		for(index = 0; index < 0x1FFFFFFF; index++);
//////		for(index = 0; index < 0x1FFFFFFF; index++);
////	
//////  RTC_InitTypeDef   RTC_InitStructure;
//////  RTC_TimeTypeDef   RTC_TimeStructure;
//////  RTC_AlarmTypeDef  RTC_AlarmStructure;
//// 


////	
////////  /* Allow access to RTC */
////////  PWR_BackupAccessCmd(ENABLE);

/////////* The RTC Clock may varies due to LSI frequency dispersion. */   
////////  /* Enable the LSI OSC */ 
////////  RCC_LSICmd(ENABLE);

////////  /* Wait till LSI is ready */  
////////  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
////////  {
////////  }

////////  /* Select the RTC Clock Source */
////////  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

////////  /* Enable the RTC Clock */
////////  RCC_RTCCLKCmd(ENABLE);

////////  /* Wait for RTC APB registers synchronisation */
////////  RTC_WaitForSynchro();

////	
////	
////	
////  /* Configure all GPIO as analog to reduce current consumption on non used IOs */
////  /* Enable GPIOs clock */
//////  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC 
//////                          | RCC_AHBPeriph_GPIOF , ENABLE);

//////  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
//////  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//////  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
//////  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
//////	
//////	GPIO_Init(GPIOA, &GPIO_InitStructure);
//////  GPIO_Init(GPIOB, &GPIO_InitStructure);
//////  GPIO_Init(GPIOC, &GPIO_InitStructure);
//////  GPIO_Init(GPIOF, &GPIO_InitStructure);





/////************************atul**************************************************************/

//////  /* Enable GPIOA clock */
//////  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

//////  /* Enable SYSCFG clock */
//////  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
//////  
//////  /* Configure PA1 pin as input floating */
//////  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
//////  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
//////  GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_1;
//////  GPIO_Init(GPIOA, &GPIO_InitStructure);


//////  /* Connect EXTI Line1 to PA1 pin */
//////  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource1);

//////  /* Configure EXTI Line1 */
//////  EXTI_InitStructure.EXTI_Line = EXTI_Line1;
//////  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//////  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
//////  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//////  EXTI_Init(&EXTI_InitStructure);

//////  /* Enable and set EXTI Line2 Interrupt to the Second priority */
//////  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_1_IRQn;
//////  NVIC_InitStructure.NVIC_IRQChannelPriority = 0x00;
////// // NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
//////  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//////  NVIC_Init(&NVIC_InitStructure);
//////	
//////	
//////	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
//////  EXTI_InitStructure.EXTI_Line = EXTI_Line1; // Line1
//////  EXTI_InitStructure.EXTI_Mode =  EXTI_Mode_Interrupt; // Interrupt mode
//////  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising; // Detect falling edge of the external signal connected to PA1
//////  EXTI_InitStructure.EXTI_LineCmd = ENABLE; // Enable EXTI line
//////  EXTI_Init(&EXTI_InitStructure); // Send parameters to the registers

//////  /* Disable GPIOs clock */
//////  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA |RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC |				//atul
//////                         RCC_AHBPeriph_GPIOD | RCC_AHBPeriph_GPIOF, DISABLE);
////  
//////  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
//////  RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
//////  RTC_InitStructure.RTC_SynchPrediv = 0x0138;
//////  
//////  if (RTC_Init(&RTC_InitStructure) == ERROR)
//////  {
//////    while(1);
//////  }
////    
//////  /* EXTI configuration */
//////  EXTI_ClearITPendingBit(EXTI_Line17);
//////  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
//////  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//////  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
//////  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//////  EXTI_Init(&EXTI_InitStructure);
//////  
//////  /* NVIC configuration */
//////  NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
//////  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//////  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//////  NVIC_Init(&NVIC_InitStructure);
////  
////  /* Set the alarm X+5s */
//////  RTC_AlarmStructure.RTC_AlarmTime.RTC_H12     = RTC_H12_AM;
//////  RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours   = 0x01;
//////  RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 0x00;
//////  RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = 0x05;
//////  RTC_AlarmStructure.RTC_AlarmDateWeekDay = 0x31;
//////  RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
//////  RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;
//////  RTC_SetAlarm(RTC_Format_BCD, RTC_Alarm_A, &RTC_AlarmStructure);
//////  
//////  /* Enable the alarm */
//////  RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
//////  
//////  /* Enable the RTC Alarm A interrupt */
//////  RTC_ITConfig(RTC_IT_ALRA, ENABLE);
//////  
//////  /* Set the time to 01h 00mn 00s AM */
//////  RTC_TimeStructure.RTC_H12     = RTC_H12_AM;
//////  RTC_TimeStructure.RTC_Hours   = 0x01;
//////  RTC_TimeStructure.RTC_Minutes = 0x00;
//////  RTC_TimeStructure.RTC_Seconds = 0x00;  
//////  
//////  RTC_SetTime(RTC_Format_BCD, &RTC_TimeStructure);
//////  
//////  /* Clear the Alarm A Pending Bit */
//////  RTC_ClearITPendingBit(RTC_IT_ALRA);  

////	stop_mode_exit = 1;
////  /* Enter Stop Mode */
////  PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);  //		PWR_Regulator_LowPower


////}



void StopMode_Measure(void)
{
  __IO uint32_t index = 0;
  GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef  NVIC_InitStructure;
  EXTI_InitTypeDef  EXTI_InitStructure;
	
	
  /* Disable the USB interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USB_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = USB_IT_PRIO;
	NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	
	/* Enable PWR APB1 Clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	ADC_DeInit(ADC1);
	
	 DCD_EP_Close(dev_mode,
              CDC_IN_EP);
  
  /* Open EP OUT */
  DCD_EP_Close(dev_mode,
              CDC_OUT_EP);
  
  /* Open Command IN EP */
  DCD_EP_Close(dev_mode,
              CDC_CMD_EP);

  /* Restore default state of the Interface physical components */
  APP_FOPS.pIf_DeInit();
	//DCD_EP_Close(dev_mode, 0);
	//DCD_EP_Close(dev_mode, 0x80);
	USBD_Suspend(dev_mode);
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA , ENABLE); 
	
	DCD_DevDisconnect(dev_mode);
	DCD_StopDevice(dev_mode);
	USBD_DeInit(dev_mode);
	RCC_USBCLKConfig(RCC_USBCLK_HSI48); 
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, DISABLE);
	
	memset(dev_mode, sizeof(USB_Device_dev), 0);
	
	
  /* Enable and set EXTI Line2 Interrupt to the Second priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0x00;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);	
	/* Configure EXTI Line13 */
  EXTI_InitStructure.EXTI_Line = EXTI_Line13;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
	
	
	
  /* Configure all GPIO as analog to reduce current consumption on non used IOs */
  /* Enable GPIOs clock */
  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC |    // 
                           RCC_AHBPeriph_GPIOF , ENABLE);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
	
	GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  //GPIO_Init(GPIOC, &GPIO_InitStructure);
  GPIO_Init(GPIOF, &GPIO_InitStructure);

	
	 /* Disable GPIOs clock */
  RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB  | RCC_AHBPeriph_GPIOF, DISABLE);   //
// RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOB  | RCC_AHBPeriph_GPIOF, DISABLE); 

	 /*Low Power Sleep on Exit Disabled*/
 // NVIC_SystemLPConfig(NVIC_LP_SLEEPDEEP, ENABLE);



/************************atul**************************************************************/

//  /* Enable GPIOA clock */
//  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

//  /* Enable SYSCFG clock */
//  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
//  
//  /* Configure PA1 pin as input floating */
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
//  GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_1;
//  GPIO_Init(GPIOA, &GPIO_InitStructure);


//  /* Connect EXTI Line1 to PA1 pin */
//  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource1);

//  /* Configure EXTI Line1 */
//  EXTI_InitStructure.EXTI_Line = EXTI_Line1;
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);

//  /* Enable and set EXTI Line2 Interrupt to the Second priority */
//  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_1_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPriority = 0x00;
// // NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
//	
//	
//	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
//  EXTI_InitStructure.EXTI_Line = EXTI_Line1; // Line1
//  EXTI_InitStructure.EXTI_Mode =  EXTI_Mode_Interrupt; // Interrupt mode
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising; // Detect falling edge of the external signal connected to PA1
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE; // Enable EXTI line
//  EXTI_Init(&EXTI_InitStructure); // Send parameters to the registers

//  /* Disable GPIOs clock */
//  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA |RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC |				//atul
//                         RCC_AHBPeriph_GPIOD | RCC_AHBPeriph_GPIOF, DISABLE);
  
//  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
//  RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
//  RTC_InitStructure.RTC_SynchPrediv = 0x0138;
//  
//  if (RTC_Init(&RTC_InitStructure) == ERROR)
//  {
//    while(1);
//  }
    
//  /* EXTI configuration */
//  EXTI_ClearITPendingBit(EXTI_Line17);
//  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);
//  
//  /* NVIC configuration */
//  NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
  
  /* Set the alarm X+5s */
//  RTC_AlarmStructure.RTC_AlarmTime.RTC_H12     = RTC_H12_AM;
//  RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours   = 0x01;
//  RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 0x00;
//  RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = 0x05;
//  RTC_AlarmStructure.RTC_AlarmDateWeekDay = 0x31;
//  RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
//  RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;
//  RTC_SetAlarm(RTC_Format_BCD, RTC_Alarm_A, &RTC_AlarmStructure);
//  
//  /* Enable the alarm */
//  RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
//  
//  /* Enable the RTC Alarm A interrupt */
//  RTC_ITConfig(RTC_IT_ALRA, ENABLE);
//  
//  /* Set the time to 01h 00mn 00s AM */
//  RTC_TimeStructure.RTC_H12     = RTC_H12_AM;
//  RTC_TimeStructure.RTC_Hours   = 0x01;
//  RTC_TimeStructure.RTC_Minutes = 0x00;
//  RTC_TimeStructure.RTC_Seconds = 0x00;  
//  
//  RTC_SetTime(RTC_Format_BCD, &RTC_TimeStructure);
//  
//  /* Clear the Alarm A Pending Bit */
//  RTC_ClearITPendingBit(RTC_IT_ALRA);  

	stop_mode_exit = 1;
  /* Enter Stop Mode */
  PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);  //		PWR_Regulator_LowPower
//		SYSCLKConfig_STOP();	
//		USBD_Reset(dev_mode);
//		usbd_init();				
//		Init_Target ();			   //Initialize all hardware peripherals 
  			

//	SYSCLKConfig_STOP();	
//  stop_mode_exit = 0; 
//	_SetCNTR(IMR_MSK);
//	//SystemInit();
//	
//	Init_Target ();			   //Initialize all hardware peripherals 
//	usbd_init();					 //Initialize USB in device mode	


}


/**
  * @brief  This function configures the system to enter Standby mode for
  *         current consumption measurement purpose.
  *         STANDBY Mode
  *         ============
  *           - RTC OFF
  *           - IWDG and LSI OFF
  *           - Wakeup using WakeUp Pin2 (PC.13)
  * @param  None
  * @retval None
  */
void StandbyMode_Measure(void)
{
  /* Disable wake-up source(Wake up pin) to guarantee free access to WUT level-OR input */ 
  PWR_WakeUpPinCmd(PWR_WakeUpPin_2, DISABLE);  
  
  /* Clear Standby flag */
  PWR_ClearFlag(PWR_FLAG_SB);
    
  /* Clear Power Wake-up (CWUF) flag */
  PWR_ClearFlag(PWR_FLAG_WU);

  /* Enable WKUP pin 2 */
  PWR_WakeUpPinCmd(PWR_WakeUpPin_2,ENABLE);
  
  /* Request to enter STANDBY mode (Wake Up flag is cleared in PWR_EnterSTANDBYMode function) */
  PWR_EnterSTANDBYMode();
  
}

/**
  * @brief  This function configures the system to enter Standby mode with RTC 
  *         clocked by LSI for current consumption measurement purpose.
  *         STANDBY Mode with RTC clocked by LSI
  *         ========================================
  *           - RTC Clocked by LSI
  *           - IWDG OFF
  *           - Automatic Wakeup using RTC 
  * @param  None
  * @retval None
  */


uint8_t Low_Power_Mode_Check()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;
  EXTI_InitTypeDef  EXTI_InitStructure;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType= GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;    
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_Level_3;

	


	/*********** LPM PC13 CONFIGURATION ****************************/
	
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_Level_1;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13  ;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	vsense_result = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13);
	
	return vsense_result;
}

void StandbyRTCMode_Measure(void)
{
  RTC_InitTypeDef   RTC_InitStructure;
  RTC_AlarmTypeDef  RTC_AlarmStructure;
  RTC_TimeTypeDef   RTC_TimeStructure;
  
  /* Allow access to RTC */
  PWR_BackupAccessCmd(ENABLE);
  
  /* The RTC Clock may varies due to LSI frequency dispersion. */   
  /* Enable the LSI OSC */ 
  RCC_LSICmd(ENABLE);
  
  /* Wait till LSI is ready */  
  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {
  }
  
  /* Select the RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
  
  /* Enable the RTC Clock */
  RCC_RTCCLKCmd(ENABLE);
  
  /* Wait for RTC APB registers synchronisation */
  RTC_WaitForSynchro();
  
  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
  RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
  RTC_InitStructure.RTC_SynchPrediv = 0x0138;
  
  RTC_Init(&RTC_InitStructure);
  
  /* Set the alarm X+5s */
  RTC_AlarmStructure.RTC_AlarmTime.RTC_H12     = RTC_H12_AM;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours   = 0x01;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 0x00;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = 0x08;
  RTC_AlarmStructure.RTC_AlarmDateWeekDay = 0x31;
  RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
  RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;
  RTC_SetAlarm(RTC_Format_BCD, RTC_Alarm_A, &RTC_AlarmStructure);
    
  /* Enable the alarm */
  RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
  
  /* Set the time to 01h 00mn 00s AM */
  RTC_TimeStructure.RTC_H12     = RTC_H12_AM;
  RTC_TimeStructure.RTC_Hours   = 0x01;
  RTC_TimeStructure.RTC_Minutes = 0x00;
  RTC_TimeStructure.RTC_Seconds = 0x00;  
  
  RTC_SetTime(RTC_Format_BCD, &RTC_TimeStructure);
  
  /* Disable RTC Alarm A Interrupt */
  RTC_ITConfig(RTC_IT_ALRA, DISABLE);   
  
  /* Clear Power Wake-up (CWUF) flag */
  PWR_ClearFlag(PWR_FLAG_WU);  
  
  /* Enable RTC Alarm A Interrupt */
  RTC_ITConfig(RTC_IT_ALRA, ENABLE);  
  
  RTC_ClearFlag(RTC_FLAG_ALRAF);
  
  /* Request to enter STANDBY mode (Wake Up flag is cleared in PWR_EnterSTANDBYMode function) */
  PWR_EnterSTANDBYMode();
  
  /* Infinite loop */
  while (1)
  {
  }
}

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
