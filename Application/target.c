/********************************************************************************************************************************
 * File name:	 	target.c
 *
 * Attention:		Copyright 2015 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 22/12/2015 by Sonam
 *
 * Description: 	This module is used to initialize all peripheral on Cool Smart.
 *******************************************************************************************************************************/


/********************************************************************************************************************************
 * Include Section
 *******************************************************************************************************************************/
#include "stm32f0xx.h"
#include "uart.h"
#include "timer.h"
#include "adc.h"
#include "i2c.h"
#include "eeprom.h"
#include "led.h"
#include "pin_config.h"
#include "watchdog.h"
#include "stm32f0xx_rcc.h"
#include "button_input.h"
#include "std_periph_headers.h"
#include "profile.h"
#include "rtc.h"
#include "water_management.h"
#include "buzzer.h"
#include "memory_map.h"
#include "json_client.h"
#include "client_registration.h"
#include "m-35_config.h"
#include "schedule.h"
#include "buyers_n_client_reg.h"
#include "target.h"
/********************************************************************************************************************************
* Defines section
 *******************************************************************************************************************************/

/********************************************************************************************************************************
* Global declarations section
 *******************************************************************************************************************************/
extern button device_mode;
extern device_profile profile;
uint8_t di_return = 0;
extern uint8_t  OHT_triggered, UGT_triggered, module_count;

extern struct buzzer_switch_behavior buzzer_switch_info;
extern struct buzzer_interval 	buzzer_interval_info;

extern volatile uint8_t wifi_initialised, schedule_enable_flag;

extern uint32_t adc_channel_type;

extern uint16_t day_time_in_minutes_glbl;
extern RTC_DateTypeDef date_obj, RTC_DateStruct;
extern RTC_TimeTypeDef RTC_TimeStructure;

extern struct wms_sys_info wms_sys_config;
#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif
extern struct loc_tank_trigger_flags trigger_state;


extern 	uint16_t buzzer_activation_counter;//atul
extern 	uint8_t buzzer_activation_flag;

extern 	volatile uint8_t notification_logno;

extern struct wifi_init_params  wifi_param_obj;

uint8_t time[] = {0,17,11,4,1};

extern uint8_t *nw_strings[2];  // Assid, Akey



/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */

void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
	NVIC_SystemReset();
}



/********************************************************************************************************************************
 * Function name: 	void init_target(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		
 *
 * Date created: 	
 *
 * Description: 	Initializes all the modules present in Cool Smart.
 *
 * Notes:
 *******************************************************************************************************************************/
void Init_Target(void){
 uint8_t profile_data[100] , read_buff[100], count, indx, loc, buff[20], temp_arr[50], perform_default_wr;
  uint32_t loc_var, level = 0 ;
	uint8_t len, idx, ssid_key_len;


	/**********************************************************/
	/*			 Initialize led's																  */
	/**********************************************************/

	#ifdef LED_MODULE_ENABLE
		led_Init();
	#endif

	/**********************************************************/
	/*			 Initialize Relay at PA0 && PA1 				 					 */
	/**********************************************************/

	 RELAY_OHT_Init();

	/**********************************************************/
	/*	 Initialize Digital Inputs PB0-PB-15 && PC5-PC7 	 		*/
	/**********************************************************/
	
	init_Digitalinputs ();


	/**********************************************************/
	/*			Initialize Button on Ext Interrupt 			  				*/
	/**********************************************************/
	
	 EXTILine2_Config();	// Configure External Input PUMP1Button as Input
 
 
	/**********************************************************/
	/*					   Initialize EEPROM 				 									*/
	/**********************************************************/
	
	 #ifdef EEPROM_MODULE_ENABLE
		i2c_init ();
		EEPROM_WP_init();
	 #endif
	 
	 
	/**********************************************************/		
	/*	Initialize Timers 				  													*/
	/**********************************************************/

	timer_init (TIM3, TIME_MILLI_SEC, 1000);	//Sensor control
  TIM_Cmd (TIM3, ENABLE);

	timer_init(TIM16,TIME_MILLI_SEC, 10);     //Led Display control
  TIM_Cmd (TIM16, ENABLE);

	timer_init(TIM14,TIME_MILLI_SEC, 1000);    //Buzzer Control
	TIM_Cmd (TIM14, ENABLE);
	
//	timer_init(TIM17,TIME_MICRO_SEC, 15);    //Buzzer Control   //atul
//	TIM_Cmd (TIM17, ENABLE);


	/**********************************************************/
	/*					   Initialize UARTS 				  								*/
	/**********************************************************/

		#ifdef E4
			at_command_mode();
			uart_init (USART2, 115200);		
		#endif

  

	/**********************************************************/
	/*			Initialize ADC for Feedback & Version 		  */
	/**********************************************************/

	ADC_1_Config();
	
		
	/**********************************************************/
	/*			Initialize RTC                      		  */
	/**********************************************************/

	RTC_initialize();
  

	/**********************************************************/
	/*			get Hardware version No                     		  */
	/**********************************************************/
	
	adc_channel_type = ADC_Channel_1;
	select_feedback_chnl();
	

	/**********************************************************/
	/*				set alarm					  														*/
	/**********************************************************/
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		  				// read the RTC time structure
	RTC_GetDate(RTC_Format_BIN, &date_obj);					  				// read the RTC date structure
	day_time_in_minutes_glbl = (RTC_TimeStructure.RTC_Hours*60) + RTC_TimeStructure.RTC_Minutes;	 // calculate time in minutes
//  current_day_num = get_day_num(date_obj.RTC_Date, date_obj.RTC_Month, date_obj.RTC_Year);
//	prev_day_num = current_day_num;
//	loc_var = ((day_time_in_minutes_glbl / 60) + 1)* 60;
//	set_alarm_in_rtc(loc_var);



  /**********************************************************/
	/*			Read 96-bit UID                    		  */
	/**********************************************************/
	
		ReadUID();

  /**********************************************************/
	/*		   Initialize global structure			  		  */
	/**********************************************************/

	 initialize_global_structures();


	/**********************************************************/
	/*		   Default client registration			  		  */
	/**********************************************************/

	
  eeprom_data_read_write(APPL_DEFAULT_WR_CHECK_ADDR, READ_OP, &perform_default_wr, 1);
	if(perform_default_wr != 'Y')
	 {
			loc	= 'N';	   	
			eeprom_data_read_write(SYSTEM_INFO_ADDR, WRITE_OP, &loc, 1);    /* board status */
			temp_arr[0] = '\0';
			for(count = 0; count < TOTAL_REG_KEY; count++){
				eeprom_data_read_write(DEVICE_REG_START_ADDR + count*DEVICE_REG_LEN, WRITE_OP, &temp_arr[0], 1);
			}
			clear_all_registered_clients();
		 // write_one_time_settings();     // default license no,Serial no, Mac Id not required
			write_default_configuration();
			save_default_schedules();
		//clear_consumption_logs();
		
			clear_all_notification_logs();
					
			wifi_param_obj.mode = M35_MODE_AP;	// Default mode is AP for m35 wifi module.
			eeprom_data_read_write(M35_MODE_ADDR, WRITE_OP, &wifi_param_obj.mode, 1);
			
			perform_default_wr = 'Y';
			eeprom_data_read_write(APPL_DEFAULT_WR_CHECK_ADDR, WRITE_OP, &perform_default_wr, 1);
	 }
	 
	 #ifdef FLAG_WRITE_VER_GREATER_THAN_0_1_27
		perform_default_wr = 0;
		eeprom_data_read_write(SETTINGS_WRITE_VER_GREATER_THAN_0_1_27, READ_OP, &perform_default_wr, 1);
		if(perform_default_wr != 'Y'){
				loc = 0;
				eeprom_data_read_write(SCHEDULE_FLAG_ADDR, WRITE_OP,(uint8_t *)&loc, 1);
				perform_default_wr = 'Y';
				eeprom_data_read_write(SETTINGS_WRITE_VER_GREATER_THAN_0_1_27, WRITE_OP, &perform_default_wr, 1);
		}
	 #endif


	initialize_global_structures();
	 
	 
	#ifdef E4
  module_count = 3;

  GPIO_ResetBits(GPIOB, GPIO_Pin_7);
	while((module_count != 0) && (Low_Power_Mode_Check() == 1));
	if(Low_Power_Mode_Check() == 0)
	{
		return;
	}
	else
	{
		GPIO_SetBits(GPIOB, GPIO_Pin_7);
	}

	
	module_count = 5;
	while((module_count != 0) && (Low_Power_Mode_Check() == 1));
  if(Low_Power_Mode_Check() == 0)
	{
		return;
	}
	else
	{

	}
	
	module_count = 4;
	GPIO_ResetBits(GPIOB, GPIO_Pin_6);
	while((module_count != 0) && (Low_Power_Mode_Check() == 1));
  if(Low_Power_Mode_Check() == 0)
	{
		return;
	}
	else
	{
		  GPIO_SetBits(GPIOB, GPIO_Pin_6);
	}

	
	module_count = 3;
	while((module_count != 0) && (Low_Power_Mode_Check() == 1));
	  	if(Low_Power_Mode_Check() == 0)
	{
		return;
	}
	else
	{

	} 
	
//---------------------------------------------------------------------------------------
		// m35 wireless network parameters read from eeprom & update in respective structure members.
	
	eeprom_data_read_write(M35_MODE_ADDR, READ_OP, &wifi_param_obj.mode, 1);	// mode = AP/STA
	
	if(wifi_param_obj.mode == M35_MODE_AP)
	{
		strcpy(wifi_param_obj.nw_Assid, nw_strings[0]);
		strcpy(wifi_param_obj.nw_AKey, nw_strings[1]);
	}
	else
	{
		len = 0;
		idx = 0;
		memset(&temp_arr[0], 0x00,sizeof(temp_arr));
		eeprom_data_read_write(M35_STA_MODE_SSID_START_ADDR, READ_OP, &temp_arr[0], M35_SSID_MAXLEN);	
		eeprom_data_read_write(M35_STA_MODE_SSID_LEN_ADDR, READ_OP, &ssid_key_len, 1);
		strncpy(&wifi_param_obj.nw_Sssid[0], &temp_arr[0], ssid_key_len);
		
		len = 0;
		idx = 0;
		ssid_key_len = 0;
		memset(&temp_arr[0], 0x00,sizeof(temp_arr));
	  eeprom_data_read_write(M35_STA_MODE_KEY_START_ADDR, READ_OP, &temp_arr[0], M35_KEY_MAXLEN);
		eeprom_data_read_write(M35_STA_MODE_KEY_LEN_ADDR, READ_OP, &ssid_key_len, 1);
		strncpy(&wifi_param_obj.nw_SKey[0], &temp_arr[0], ssid_key_len);
	}
	
	m_35_module_params_config(0, CONFIG_WITHOUT_PKT);
	m_35_module_params_config_cmds_send(&wifi_param_obj);
	
	#endif 
	
	
	restore_configured_settings();   
	read_pin_status();
	for(count = 0; count < wms_sys_config.total_oht; count++){								/* read real sensor */
		get_current_level(&OHT_tank[count]);	
		level += OHT_tank[count].current_level;
	}
	status.oht_current_level = (level/wms_sys_config.total_oht);
	if(wms_sys_config.total_ugt > 0){
		get_current_level(&UGT_tank);
		status.ugt_current_level =  UGT_tank.current_level;
	}
	trigger_state.prev_ugt_highest_set_sensor = UGT_tank.real_sensor_ptr->highest_set_sensor;		   // check
	for(indx = 0; indx < TOTAL_OHT_TANK; indx++){
		trigger_state.prev_oht_highest_set_sensor[indx] = OHT_tank[indx].real_sensor_ptr->highest_set_sensor;
	}
	calculate_average_level();
	#ifdef VIRTUAL_SENSOR
		calibrate_oht_virtual_system(0);
		if(wms_sys_config.total_tank > 1)
			calibrate_ugt_virtual_system(0);
	#endif
	automated_task_process();
//	if(schedule_enable_flag == 1)
//		default_schedule_fetch();
//	else
		read_schedules (OHT_tank[0].tank_config_ptr->tank_num);
	wifi_initialised = 1;
	perform_default_wr = 0;
	eeprom_data_read_write(WMS_NEW_FIRMWARE_PRESENT_ADDRESS, WRITE_OP, &perform_default_wr, 1);
}
