/********************************************************************************************************************************
 * File name:	 	BUZZER.c
 *
 * Attention:		Copyright 2014 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 31st Mar, 2014 by Sahil Saini
 *
 * Description: 	This module is used to control the Buzzer.
 *******************************************************************************************************************************/

/********************************************************************************************************************************
 * Include Section
 *******************************************************************************************************************************/
#include "buzzer.h"	
#include "string.h"
#include <stm32f0xx.h>
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "water_management.h"
//#include "AT45DB161D.h"
//#include "cli.h"
#include <RTL.h>
#include "stm32f0xx_rtc.h"
#include "uart.h"
#include"json_client.h"
#include "c_func.h"
//#include "logs.h"
#include "memory_map.h"
#include "eeprom.h"
#include "schedule.h"


/********************************************************************************************************************************
* Global Variable Declaration Section
 *******************************************************************************************************************************/
//extern expander Expander;
//buzz buzzer;
struct buzzer_switch_behavior buzzer_switch_info;
struct buzzer_interval 	buzzer_interval_info;

uint8_t	volatile beep_once_flag;
uint8_t	volatile beep_2sec;


extern volatile uint32_t driver_time_out, dry_run_periodic_check_flag, dry_run_periodic_check_counter;


//extern uint8_t buzzer_activation_flag;
//extern struct tank_info OHT_tank, UGT_tank;
uint8_t buzzer_indication_num, prev_buzzer_indication_num, buzzer_behavior[10] = {0,0, 5,60,2,10,2,20,5,60}, chk_dry_run_oht_flag, chk_dry_run_ugt_flag;
extern uint16_t dry_run_ugt_counter, dry_run_oht_counter;
extern RTC_DateTypeDef date_obj;
extern RTC_TimeTypeDef RTC_TimeStructure;
extern struct loc_tank_trigger_flags trigger_state;
extern uint32_t oht_time_bw_sensor[10],ugt_time_bw_sensor[8];
extern struct wms_sys_info wms_sys_config;
extern  uint8_t  oht_pump_trigger_cause, average_level, buzzer_set_level;
extern uint8_t volatile oht_feedback_volt, ugt_feedback_volt;
extern uint8_t json_send_arr[1000], *json_send_ptr;
extern struct auto_task auto_task_info;
extern struct tank_setting oht_tank_config[TOTAL_OHT_TANK], ugt_tank_config;
#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif

volatile uint8_t ugt_pump_trigger_flag, ugt_pump_trigger_count,  oht_pump_trigger_flag, oht_pump_trigger_count;

extern uint8_t volatile buzzer_behaviour_kill_indication_time;

extern uint8_t  dry_run_flag_schedule,dry_run_count_schedule;

extern tank_schedule oht_schd;

/*
**===========================================================================
**		global variables description
**===========================================================================
*	 buzzer_indication_num 			-  	buzzer_indication_num holds the updated serial num of buzzer pattern as per doc
	 prev_buzzer_indication_num		-	buzzer_indication_num holds the previous serial num of buzzer pattern as per doc
	 buzzer_behavior[]				-   contains the buzzer behaviour as per document 	


*/

/********************************************************************************************************************************
 * Function Prototype Section
 *******************************************************************************************************************************/

/********************************************************************************************************************************
 * Function name: 	void blow_buzzer(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	15 may, 2014
 *
 * Date modified:	None
 *
 * Description: 	This function is used to blow the buzzer.
 *
 * Notes:
 *******************************************************************************************************************************/
void blow_buzzer(uint8_t state){
	
	if(buzzer_switch_info.buzzer_activate_flag == 1){  //atul
	//	if(!(chk_exception_schedule(BUZZER_ID))){
			if(state){
				SET_BUZZER;
			//	buzzer_activation_flag = 1;	      // beep the buzzer for 1 sec
				buzzer_switch_info.buzzer_state = 1;
			}
			else{
				CLR_BUZZER;
			//	buzzer_activation_flag = 0;
				buzzer_switch_info.buzzer_state = 0;
			}	
	//	}
	}  //atul
}
/********************************************************************************************************************************
 * Function name: 	void beep_once(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	3 Nov, 2013
 *
 * Date modified:	None
 *
 * Description: 	This function is used to generate a single beep from buzzer.
 *
 * Notes:
 *******************************************************************************************************************************/
void beep_once(void){   //for 1 sec
//	beep_2sec = 1;
//	while (TIM_GetITStatus(TIM14, TIM_IT_Update) != SET);
	beep_once_flag = 1;
	blow_buzzer(BUZZER_ON);
//	while(beep_2sec);
//	blow_buzzer(BUZZER_OFF);
//	beep_once_flag = 0;
}

void buzzer_blow(void){
	
	SET_BUZZER;
	buzzer_switch_info.buzzer_state = 1;
	//os_dly_wait(40);
	CLR_BUZZER;
	buzzer_switch_info.buzzer_state = 0;
	//os_dly_wait(40);

}

/********************************************************************************************************************************
 * Function name: 	void motor_buzzer_relay_init(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil kukreja
 *
 * Date created: 	14 april 2014
 *
 * Date modified:	14 april 2014
 *
 * Description: 	this routine initialize the motor and buzzer output.
 *
 * Notes:
 *******************************************************************************************************************************/
//void motor_buzzer_relay_init(void){
//
//	GPIO_InitTypeDef GPIOstruct;
//	RCC_AHBPeriphClockCmd (MOTOR_CLOCK_PORT, ENABLE);            
//
//    printCli("\n\n\rInitializing buzzer...");            
//
//    GPIOstruct.GPIO_Pin  =  UGT_MOTOR_RELAY; 
//    GPIOstruct.GPIO_Mode = GPIO_Mode_OUT;
//    GPIOstruct.GPIO_OType = GPIO_OType_PP;
//    GPIOstruct.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(MOTOR_PORT, &(GPIOstruct));  
//
//	GPIOstruct.GPIO_Pin  =  OHT_MOTOR_RELAY; 
//    GPIOstruct.GPIO_Mode = GPIO_Mode_OUT;
//    GPIOstruct.GPIO_OType = GPIO_OType_PP;
//    GPIOstruct.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(MOTOR_PORT, &(GPIOstruct)); 
//	
//	RCC_AHB1PeriphClockCmd (BUZZER_CLOCK_PORT, ENABLE); 
//	GPIOstruct.GPIO_Pin  =  BUZZER; 
//    GPIOstruct.GPIO_Mode = GPIO_Mode_OUT;
//    GPIOstruct.GPIO_OType = GPIO_OType_PP;
//    GPIOstruct.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(BUZZER_PORT, &(GPIOstruct));   
//}
/********************************************************************************************************************************
 * Function name: 	void trigger_motor_switch(struct tank_info *tank_ptr, uint8_t motor_flag)
 *
 * parameters: 		struct tank_info *tank_ptr - pointer to the structure containing tank number.
					motor_flag	if 0  		- 	oht pump trigger
 *								   1		-	ugt pump trigger
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil kukreja
 *
 * Date created: 	14 april 2014
 *
 * Date modified:	14 april 2014
 *
 * Description: 	this routine initialize the motor and buzzer output.
 *
 * Notes:
 *******************************************************************************************************************************/
void trigger_pump(uint8_t tank_type){
	
	if(tank_type == OHT){							 // if oht pump
		if(status.oht_pump_ptr->pump_state == 1){	  // if state ON then OFF
			#ifdef SUMBERSIBLE_PUMP
				SET_UGT_MOTOR;
				ugt_pump_trigger_flag = 1;
				ugt_pump_trigger_count = 0;
				//os_dly_wait(10);
				//CLR_UGT_MOTOR;	
			#else
				CLR_OHT_MOTOR;
			#endif
			chk_dry_run_oht_flag = 0;
			dry_run_oht_counter = 0;		
			status.oht_pump_ptr->pump_state = 0;
			
			dry_run_periodic_check_flag = 0;   		//atul
			dry_run_periodic_check_counter = 0;   //atul
			if(oht_schd.schd_running_flag > 0){
					dry_run_flag_schedule = 1;
					dry_run_count_schedule = 0;
			}
		}
		else{									   // PUMP on
			#ifdef SUMBERSIBLE_PUMP
				SET_OHT_MOTOR;
				oht_pump_trigger_flag = 1;
				oht_pump_trigger_count = 0;
				//os_dly_wait(10);
				//CLR_OHT_MOTOR;	
			#else
				SET_OHT_MOTOR;
			#endif
			chk_dry_run_oht_flag = 1;
			dry_run_oht_counter = auto_task_info.tsk_1_preset_level;
			status.oht_pump_ptr->pump_state = 1;
			trigger_state.oht_motor_trigger_flag = 1;
			
			dry_run_periodic_check_flag = 0;   		//atul
			dry_run_periodic_check_counter = 0;   //atul
		}
	}
	else{												// trigger the UGT pump
		if(status.ugt_pump_ptr->pump_state == 1){	  // if state ON then OFF
			CLR_UGT_MOTOR;
			chk_dry_run_ugt_flag = 0;
			dry_run_ugt_counter = 0;		
			status.ugt_pump_ptr->pump_state = 0;
		}
		else{									   // PUMP on
			SET_UGT_MOTOR;
			chk_dry_run_ugt_flag = 1;
			dry_run_ugt_counter = auto_task_info.tsk_2_preset_level;
			status.ugt_pump_ptr->pump_state = 1;
			trigger_state.ugt_motor_trigger_flag = 1;
		}

	}
	if(buzzer_switch_info.buzzer_state == 0)
		beep_once();
}	




//void trigger_motor_switch(struct tank_info *tank_ptr, uint8_t motor_flag){
//
//	if(tank_ptr->pump_ptr->pump_state == 1){	  
//		if(motor_flag == 0){
//			#ifdef SUMBERSIBLE_PUMP
//				SET_UGT_MOTOR;
//				os_dly_wait(10);
//				CLR_UGT_MOTOR;	
//			#else
//				CLR_OHT_MOTOR;
//			#endif
//			chk_dry_run_oht_flag = 0;
//			dry_run_oht_counter = 0;
//		}
//		else{
//			CLR_UGT_MOTOR;	
//			chk_dry_run_ugt_flag = 0;
//			dry_run_ugt_counter = 0;
//		}
//		tank_ptr->pump_ptr->pump_state = 0;
//	}
//	else{			
//		if(motor_flag == 0){
//			#ifdef SUMBERSIBLE_PUMP
//				SET_OHT_MOTOR;
//				os_dly_wait(10);
//				CLR_OHT_MOTOR;	
//			#else
//				SET_OHT_MOTOR;
//			#endif
//			chk_dry_run_oht_flag = 1;
//			dry_run_oht_counter = auto_task_info.tsk_1_preset_level;
//		//	dry_run_oht_counter = 15 + oht_time_bw_sensor[tank_ptr->real_sensor_ptr->highest_set_sensor];
//		}
//		else{
//			SET_UGT_MOTOR;	
//			chk_dry_run_ugt_flag = 1;
//			dry_run_ugt_counter = auto_task_info.tsk_2_preset_level; 
//		//	dry_run_ugt_counter = 15 + ugt_time_bw_sensor[tank_ptr->real_sensor_ptr->highest_set_sensor];
//		}
//		tank_ptr->pump_ptr->pump_state = 1;
//	}
//	if(motor_flag == 0)
//		trigger_state.oht_motor_trigger_flag = 1;
//	else
//		trigger_state.ugt_motor_trigger_flag = 1;
//	
//	if(buzzer_switch_info.buzzer_state == 0)
//		beep_once();
//	/* Create a log */
//
//	/* send packet to GUI  */
//
//}
/************************************************************************//**
*		       void buzzer_behavior_select(void)
*
* @brief		This function selects the buzzer pattern.
*
* param                 None
*
*Return                none 
*
* @author			Nikhil Kukreja
* @date				06/07/14
* @note			
****************************************************************************/
 
void buzzer_behavior_select(void){
	uint8_t loc_flag = 0;

	//buzzer_indication_num = 0;



	if(buzzer_switch_info.buzzer_activate_flag == BUZZER_ON){
		if(average_level <= buzzer_set_level){
			buzzer_indication_num = 1;
			loc_flag = 1;
		}
		if(status.oht_current_level == 0){
			buzzer_indication_num = 2;
			loc_flag = 1;
		}
		if(UGT_tank.current_level < 10 && wms_sys_config.total_ugt > 0){
			buzzer_indication_num = 4;
			loc_flag = 1;
		}
		if(UGT_tank.current_level  == 0 && wms_sys_config.total_ugt > 0){
			buzzer_indication_num = 3;
			loc_flag = 1;
		}  
		if(loc_flag == 0)
			buzzer_indication_num = 0;
				 	
		if(buzzer_indication_num != prev_buzzer_indication_num){
			
			buzzer_behaviour_kill_indication_time = 0;
			
			buzzer_interval_info.beep_time = buzzer_behavior[buzzer_indication_num * 2];
			buzzer_interval_info.interval = buzzer_behavior[(buzzer_indication_num*2) + 1];			
			prev_buzzer_indication_num = buzzer_indication_num;
			blow_buzzer(OFF); 
		}
	}
}
/************************************************************************//**
*		      uint8_t actuate_motor(struct tank_info *tank_ptr, uint8_t state)
*
* @brief		This function actuate the pump.
*
* param         struct tank_info *tank_ptr	- pointer to the structure containing tank info
*				state						- state to be trigger.
*
*Return                 
*
* @author			Nikhil Kukreja
* @date				06/07/14
* @note			
****************************************************************************/
 
uint8_t actuate_motor(uint8_t tank_type, uint8_t state){

	if(tank_type == OHT){							 // if oht pump
		if(state == status.oht_pump_ptr->pump_state){
			return 0;
		}
		if(status.oht_pump_ptr->pump_state == 1){	  // if state ON then OFF
			#ifdef SUMBERSIBLE_PUMP
				SET_UGT_MOTOR;
				ugt_pump_trigger_flag = 1;
				ugt_pump_trigger_count = 0;
			//	os_dly_wait(10);
			//	CLR_UGT_MOTOR;	
			#else
				CLR_OHT_MOTOR;
			#endif
			chk_dry_run_oht_flag = 0;
			dry_run_oht_counter = 0;		
			status.oht_pump_ptr->pump_state = 0;
			
			dry_run_periodic_check_flag = 0;   		//atul
			dry_run_periodic_check_counter = 0;   //atul
		}
		else{									   // PUMP off
			#ifdef SUMBERSIBLE_PUMP
				SET_OHT_MOTOR;
				oht_pump_trigger_flag = 1;
				oht_pump_trigger_count = 0;
				//os_dly_wait(10);
				//CLR_OHT_MOTOR;	
			#else
				SET_OHT_MOTOR;
			#endif
			chk_dry_run_oht_flag = 1;
			dry_run_oht_counter = auto_task_info.tsk_1_preset_level;
			status.oht_pump_ptr->pump_state = 1;
			trigger_state.oht_motor_trigger_flag = 1;
			
			dry_run_periodic_check_flag = 0;   		//atul
			dry_run_periodic_check_counter = 0;   //atul
		}
	}
	else{												// trigger the UGT pump
		if(state == status.ugt_pump_ptr->pump_state){
			return 0;
		}
		if(status.ugt_pump_ptr->pump_state == 1){	  // if state ON then OFF
			CLR_UGT_MOTOR;
			chk_dry_run_ugt_flag = 0;
			dry_run_ugt_counter = 0;		
			status.ugt_pump_ptr->pump_state = 0;
		}
		else{									   // PUMP on
			SET_UGT_MOTOR;
			chk_dry_run_ugt_flag = 1;
			dry_run_ugt_counter = auto_task_info.tsk_2_preset_level;
			status.ugt_pump_ptr->pump_state = 1;
			trigger_state.ugt_motor_trigger_flag = 1;
		}

	}
	if(buzzer_switch_info.buzzer_state == 0)
		beep_once();	
   return 1;
}

//	if(tank_ptr->pump_ptr->pump_state == state){
//		return 0;	
//	}
//	else{
//		if(state){
//			if(tank_ptr->tank_config_ptr->tank_num <= 25){
//				#ifdef SUMBERSIBLE_PUMP
//					SET_OHT_MOTOR;
//					os_dly_wait(10);
//					CLR_OHT_MOTOR;	
//				#else
//					SET_OHT_MOTOR;
//				#endif
//				trigger_state.oht_motor_trigger_flag = 1;
//				chk_dry_run_oht_flag = 1;
//				dry_run_oht_counter = auto_task_info.tsk_1_preset_level; 
//				
//		//		dry_run_oht_counter = 15 + oht_time_bw_sensor[tank_ptr->real_sensor_ptr->highest_set_sensor];
//			}
//			else{
//				SET_UGT_MOTOR;
//				trigger_state.ugt_motor_trigger_flag = 1;
//				chk_dry_run_ugt_flag = 1;
//				dry_run_ugt_counter = auto_task_info.tsk_2_preset_level; 
//				//	dry_run_ugt_counter = 15 + ugt_time_bw_sensor[tank_ptr->real_sensor_ptr->highest_set_sensor];
//			}
//		}
//		else{
//			if(tank_ptr->tank_config_ptr->tank_num <= 25){
//				#ifdef SUMBERSIBLE_PUMP
//					SET_UGT_MOTOR;
//					os_dly_wait(10);
//					CLR_UGT_MOTOR;	
//				#else
//					CLR_OHT_MOTOR;
//				#endif
//				trigger_state.oht_motor_trigger_flag = 1;
//				chk_dry_run_oht_flag = 0;
//			}
//			else{
//				CLR_UGT_MOTOR;
//				trigger_state.ugt_motor_trigger_flag = 1;
//				chk_dry_run_ugt_flag = 0;
//			}
//		}
//		tank_ptr->pump_ptr->pump_state = state;	
//	}
//	return 1;
//}

/************************************************************************//**
*		    void update_dry_run_counter()
*
* @brief		This function update the counter for pump dry run.
*
* param         		none
*
*Return                none 
*
* @author			Nikhil Kukreja
* @date				06/07/14
* @note			
****************************************************************************/
//void update_dry_run_counter(){
//	
//	if((trigger_state.prev_oht_highest_set_sensor < OHT_tank.real_sensor_ptr->highest_set_sensor) && status.oht_pump_ptr->pump_state == 1){	
////		dry_run_oht_counter = 15 + oht_time_bw_sensor[OHT_tank.real_sensor_ptr->highest_set_sensor];	
//	}
//	if((trigger_state.prev_ugt_highest_set_sensor < UGT_tank.real_sensor_ptr->highest_set_sensor) && status.ugt_pump_ptr->pump_state == 1){	
////		dry_run_ugt_counter = 15 + ugt_time_bw_sensor[UGT_tank.real_sensor_ptr->highest_set_sensor];
//	}
//}

/************************************************************************//**
*		   uint32_t pump_trigger_save_status(uint8_t tank_num){
*
* @brief		This function will trigger the pump .
*
* param         tank_num - pump trigger of corresponding tank.
*
*Return         ret_tank_status - status in bits that tell wheather pump is trigger or not.
*					if 0th bit is set - pump is on 	
*					return the error number in bits
*			
*
* @author			Nikhil Kukreja
* @date				06/07/14
* @note			
****************************************************************************/

uint32_t pump_trigger_save_status(uint8_t tank_num){
	uint32_t ret_tank_status = 0;
	
	if(tank_num <= 25){
		if(oht_feedback_volt != 0){   									/* oht wire connected */
			//trigger_motor_switch(status->oht_pump_ptr, 0);							  /* trigger the motor status */
			trigger_pump(OHT);
			ret_tank_status |= ((uint32_t)1<< PUMP_TRIGGER);	
		}
		else{
			ret_tank_status  |= ((uint32_t)1<< (PUMP_CANT_START_WIRE_DISCONNECT));		  	/* wire disconnected event */
		}
	}
	else if(tank_num > 25 && tank_num < 50){
		if(wms_sys_config.total_ugt > 0){
			if(wms_sys_config.ugt_pump_select == 1){
				if(ugt_feedback_volt != 0){   									/* oht wire connected */
				//	trigger_motor_switch(&UGT_tank, 1);							  /* trigger the motor status */
					trigger_pump(UGT);
					ret_tank_status |= ((uint32_t)1<< PUMP_TRIGGER);	
				}
				else{
					ret_tank_status  |= (1<< (PUMP_CANT_START_WIRE_DISCONNECT));		  	/* wire disconnected event */
				}
			}
		}
	}
	return ret_tank_status;
}


void trigger_motor(uint8_t tank_num){
	uint32_t ret_tank_status = 0;
	
	if(tank_num >= 1 && tank_num <= 25){
		trigger_pump(OHT);
	}
	else if(tank_num > 25 && tank_num < 50){
		trigger_pump(UGT);
	}
}



/************************************************************************//**
*		  void save_buzzer_setting(struct json_struct *wms_payload_info, uint8_t num_bytes){
*
* @brief		This function save the buzzer settings.
*
* param         struct tank_info *tank_ptr -  pointer to the structure containing tank info
*			   						num_bytes - packet length 
*Return                none 
*
* @author			Nikhil Kukreja
* @date				05/12/14
* @note			
*************************************************************************************/

void save_buzzer_setting(struct json_struct *wms_payload_info, uint8_t num_bytes){
	uint8_t total_length, read_arr[10];

	buzzer_switch_info.buzzer_activate_flag = wms_payload_info->data[1] - 0x30;
	if(buzzer_switch_info.buzzer_activate_flag == 1){
		total_length = extract_json_field_val(&read_arr[0], &wms_payload_info->data[3]);
		buzzer_switch_info.buzzer_snooze_dur = ascii_decimal(&read_arr[0], total_length);	
		//buzzer_switch_info.buzzer_snooze_dur *= 60; 
		total_length = extract_json_field_val(&read_arr[0], &wms_payload_info->data[3 + total_length +2]);
		buzzer_set_level = ascii_decimal(&read_arr[0], total_length);	
	}
	else if(buzzer_switch_info.buzzer_activate_flag == 2){		// snooze the buzzer
	//	blow_buzzer(OFF);
		if(buzzer_switch_info.buzzer_state == 1){
			CLR_BUZZER;
			buzzer_switch_info.buzzer_state = 0;
		}
			
		buzzer_interval_info.buzzer_snooze_flag = 1;
		return;
	}
	else{
		CLR_BUZZER;
		buzzer_switch_info.buzzer_state = 0;
	}
		
	/* save buzzer setting in memory */	
//	dflash_write_multiple_byte(BUZZER_SETTING_INFO_ADDR, (uint8_t *)&buzzer_switch_info.buzzer_activate_flag, 1);			/* buzzer activation flag */
    eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR, WRITE_OP, (uint8_t *)&buzzer_switch_info.buzzer_activate_flag, 1 );				
//	dflash_write_multiple_byte(BUZZER_SETTING_INFO_ADDR + 1, (uint8_t *)&buzzer_switch_info.buzzer_snooze_dur, 1);			/* buzzer snooze duration */
	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR + 1, WRITE_OP, (uint8_t *)&buzzer_switch_info.buzzer_snooze_dur, 1);																		
	buzzer_switch_info.buzzer_snooze_dur *= 60; 
//	dflash_write_multiple_byte(BUZZER_SETTING_INFO_ADDR + 2, (uint8_t *)&buzzer_set_level, 1);					/* buzzer blow on level */	
	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR + 2, WRITE_OP, (uint8_t *)&buzzer_set_level, 1 );																	
}





/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/








