#include <stdint.h>
#include <stdlib.h>
#include "memory_info.h"
#include "port_mapping.h"
#include "pin_config.h"
#include "eeprom.h"
#include "io_system.h"
#include "system.h"

#define TOSHIBA_VER1
//#define DIAKIN_VER2
//#define TWO_PIR_TOSHIBA_VER3

struct input_port_mapping inps[MAX_INPUT_PORTS];
struct port_param *op_port_mem_pool_head = NULL;
extern struct system_params sys_info;

/********************************************************************************************************************************
 * Function name: 	struct port_param * port_struct_addr (struct memory_port_info *find_port)
 *
 * Returns: 		uint8_t										PORT_FOUND		= 1
 *																PORT_NOT_FOUND	= 0
 *
 * Arguments: 		struct memory_port_info *find_port			output port to be searched in output ports pool.
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to attach the configured output ports from global output ports pool.
 *
 * Notes:			None
 *******************************************************************************************************************************/
uint8_t search_port_existence_in_mem_pool (struct memory_port_info *find_port) {
	struct port_param *traverse = op_port_mem_pool_head;		/* Pointer to traverse the output port pool. */
	while (traverse) {											/* While end of pool not reached. */
		if ((traverse->slave_add == find_port->slave_add) && (traverse->port == find_port->port)) {				
			return PORT_FOUND;									
		}
		traverse = traverse->next_port;							
	}
	return PORT_NOT_FOUND;
}

/********************************************************************************************************************************
 * Function name: 	struct port_param * port_struct_addr (struct memory_port_info *find_port)
 *
 * Returns: 		struct port_param *							Address of the port in output ports pool that was passed as
 *																parameter to this routine. In case the port is not found, NUll is returned.
 *
 * Arguments: 		struct memory_port_info *find_port			output port to be searched in output ports pool.
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to attach the configured output ports from global output ports pool.
 *
 * Notes:			None
 *******************************************************************************************************************************/
struct port_param * port_struct_addr (struct memory_port_info *find_port) {
	struct port_param *traverse = op_port_mem_pool_head;		/* Pointer to traverse the output port pool. */
	while (traverse) {											/* While end of pool not reached. */
		if ((traverse->slave_add == find_port->slave_add) && (traverse->port == find_port->port)) {				
			return traverse;
		}
		traverse = traverse->next_port;
	}
	return NULL;
}

/********************************************************************************************************************************
 * Function name: 	void attach_port_to_input (struct input_port_mapping *src_input, uint8_t idx, uint8_t odx)
 *
 * Returns: 		None
 *
 * Arguments: 		struct input_port_mapping *src_input		input to which the output ports are to be attached.
 *					uint8_t idx									input number.
 *					uint8_t odx									output number.
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to attach the configured output ports from global output ports pool.
 *
 * Notes:			None
 *******************************************************************************************************************************/
void attach_port_to_input (struct input_port_mapping *src_input, uint8_t idx, uint8_t odx) {
	struct ports_pool *nu_pool, *traverse_pool = src_input->port_pool;			/* Pointers to output port info pool. */
	struct memory_port_info setting;

	/* Read Port info from eeprom. */
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(idx, odx), READ_OP, (uint8_t *)&setting, sizeof (setting));

	/* Create a new port info handling point. */
	nu_pool = (struct ports_pool *)malloc (sizeof (struct ports_pool));
	nu_pool->next_port_struct = NULL;

	/* Attach to the port info from port pool. */
	nu_pool->port_struct = port_struct_addr (&setting);

	if (src_input->port_pool == NULL) {
		/* Port pool for current input does not exists. */
		src_input->port_pool = nu_pool;
	}
	else {
		while (traverse_pool->next_port_struct) {
			/* Traverse forward. */
			traverse_pool = traverse_pool->next_port_struct;
		}
		traverse_pool->next_port_struct = nu_pool;
	}
}

/********************************************************************************************************************************
 * Function name: 	void fill_port_fields (struct port_param *nu_port, struct memory_port_info *setting)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to initialize the newly created ouput port node.
 *
 * Notes:			None
 *******************************************************************************************************************************/
void fill_port_fields (struct port_param *nu_port, struct memory_port_info *setting) {
	nu_port->slave_add			=	setting->slave_add;
	nu_port->port				= 	setting->port;
	nu_port->output_port_type	=	setting->output_port_type;
	nu_port->value				=	0;
	nu_port->send_pulse			=	0;
	nu_port->pulse_state		=	PORT_STATE_OFF;
	nu_port->next_port			=	NULL; 
}

/********************************************************************************************************************************
 * Function name: 	void add_port_to_pool (struct input_port_mapping *src_input, uint8_t idx, uint8_t odx)
 *
 * Returns: 		None
 *
 * Arguments: 		uint8_t idx			Input Number.
 *					uint8_t odx			Output Number.
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to load a new output port node to the global output port pool.
 *
 * Notes:			None
 *******************************************************************************************************************************/
void add_port_to_pool (uint8_t idx, uint8_t odx) {
	/* Creates memory pool. */
	struct memory_port_info setting;
	struct port_param *nu_port, *traverse = op_port_mem_pool_head;

	/* Read the output port details for a particular input-output port duo. */
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(idx, odx), READ_OP, (uint8_t *)&setting, sizeof (setting));

	/* Check if Port Pool is Empty. */
	if (op_port_mem_pool_head == NULL) {
		nu_port = (struct port_param *)malloc (sizeof (struct port_param));
		fill_port_fields (nu_port, &setting);
		op_port_mem_pool_head =	nu_port;
	}
	else {
		/* Check if the port to be added already exists in the pool or not. */
		if (search_port_existence_in_mem_pool (&setting) == PORT_NOT_FOUND) {
			nu_port = (struct port_param *)malloc (sizeof (struct port_param));
			fill_port_fields (nu_port, &setting);
			/* Load the new port at the end of output port pool. */
			while (traverse->next_port) {
				traverse = traverse->next_port;
			}
			traverse->next_port = nu_port;
		}
	}
}

/********************************************************************************************************************************
 * Function name: 	void initialize_port_mapping (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to initialize all the input ports.
 *
 * Notes:			Called when an input is triggered by rising or falling edge.
 *					Determines if stable high or low level is sensed.
 *******************************************************************************************************************************/
void initialize_port_mapping (void) {
	uint8_t idx, odx, op_attached [MAX_INPUT_PORTS], input_config [MAX_INPUT_PORTS], input_binding_keys [MAX_INPUT_PORTS], lst_op_states [MAX_ONBOARD_OUTPUT_PORTS];

	/* Reading input config setting, which tells what type of input is there. */
	eeprom_data_read_write(INPUT_CONFIG(0), READ_OP, input_config, sizeof (input_config));
	/* Reading output config setting, which tells how many output ports are mapped with current input. */
	eeprom_data_read_write(OUTPUTS_ATTACHED(0), READ_OP, op_attached, sizeof (op_attached));
	/* Reading input binding keys, which are used to club 2 or more inputs. */
	eeprom_data_read_write(INPUT_BINDING(0), READ_OP, input_binding_keys, sizeof (input_binding_keys));
	/* Reading last saved output states, which are used to restore outputs to there previous state. */
	eeprom_data_read_write(OUTPUT_STATE_SAVED(0), READ_OP, lst_op_states, sizeof (lst_op_states));

	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		inps [idx].input_port_type						= input_config [idx];
		inps [idx].ports_active							= op_attached [idx];
		inps [idx].binding_key							= input_binding_keys [idx];
		inps [idx].pir_info								= NULL;
		inps [idx].input_debounce_counter				= 0;
		inps [idx].valid_input_sensed					= 0;
		inps [idx].prev_state = inps [idx].curr_state	= read_input_pin (idx);
	}

	/* For each input port. */
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		/* Read the details of all the output ports attached to the current input. */
		for (odx = 0; odx < inps [idx].ports_active; odx++) {
			/* Add the port to the global output port pool if it doesn't already exists. */
			add_port_to_pool (idx, odx);
		}
	}

	/* For each input port. */
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		/* Attach the output ports from the global output ports pool to there respective inputs. */
		for (odx = 0; odx < inps [idx].ports_active; odx++) {
			attach_port_to_input (&inps [idx], idx, odx);
		}
	}

	/* For each on-board output port, restore there previous state. */
	for (idx = 0; idx < MAX_ONBOARD_OUTPUT_PORTS; idx++) {
		if (lst_op_states [idx] == PORT_STATE_ON) {
			set_do ((idx + 64), PORT_STATE_ON);
		}
	}
}

#ifdef TOSHIBA_VER1
void set_default (void) {
	struct memory_port_info def;
	uint8_t input_config[] = {INPUT_PORT_TYPE_PIR, INPUT_PORT_TYPE_ON_OFF, INPUT_PORT_TYPE_FEEDBACK, INPUT_PORT_TYPE_MODE_SELECT};
	uint8_t num_of_op_atchd [] = {2, 2, 1, 0};
	uint8_t input_binding_keys [] =	{1, 1, 1, 1};
	uint8_t idx;
	uint16_t def_vals;
	
	sys_info.slave_add = 0xFF;
	eeprom_data_read_write(0, WRITE_OP, (uint8_t *)&sys_info.slave_add, 1);

	eeprom_data_read_write(INPUT_CONFIG(0), WRITE_OP, input_config, sizeof (input_config));
	eeprom_data_read_write(OUTPUTS_ATTACHED(0), WRITE_OP, num_of_op_atchd, sizeof (num_of_op_atchd));
	eeprom_data_read_write(INPUT_BINDING(0), WRITE_OP, input_binding_keys, sizeof (input_binding_keys));

	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		def_vals = DFLT_LOW_ACT_TIME;
		eeprom_data_read_write(PIR_LOW_ACTIVITY_INFO(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint16_t));
		def_vals = DFLT_MED_ACT_TIME;
		eeprom_data_read_write(PIR_MEDIUM_ACTIVITY_INFO(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint16_t));
		def_vals = DFLT_HIGH_ACT_TIME;
		eeprom_data_read_write(PIR_HIGH_ACTIVITY_INFO(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint16_t));
		def_vals = DFLT_NULL_CNT_WNDWS;
		eeprom_data_read_write(PIR_NULL_CNT_WINDOW(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint8_t));
		def_vals = DFLT_DEC_PRCNTG;
		eeprom_data_read_write(PIR_DEC_PRCNT(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint8_t));
	}


	def.slave_add = 2;
	def.port = 64; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(0,0), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 65; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO_PULSE;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(0,1), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 64; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(1,0), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 65; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO_PULSE;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(1,1), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 65; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO_PULSE;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(2,0), WRITE_OP, (uint8_t *)&def, sizeof (def));
}
#endif

#ifdef DIAKIN_VER2
void set_default (void) {
	struct memory_port_info def;
	uint8_t input_config[] = {INPUT_PORT_TYPE_PIR, INPUT_PORT_TYPE_ON_OFF, INPUT_PORT_TYPE_FEEDBACK, INPUT_PORT_TYPE_MODE_SELECT};
	uint8_t num_of_op_atchd [] = {2, 2, 1, 0};
	uint8_t input_binding_keys [] =	{1, 1, 2, 0};
	uint8_t idx;
	uint16_t def_vals;

	sys_info.slave_add = 0xFF;
	eeprom_data_read_write(0, WRITE_OP, (uint8_t *)&sys_info.slave_add, 1);

	eeprom_data_read_write(INPUT_CONFIG(0), WRITE_OP, input_config, sizeof (input_config));
	eeprom_data_read_write(OUTPUTS_ATTACHED(0), WRITE_OP, num_of_op_atchd, sizeof (num_of_op_atchd));
	eeprom_data_read_write(INPUT_BINDING(0), WRITE_OP, input_binding_keys, sizeof (input_binding_keys));

	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		def_vals = DFLT_LOW_ACT_TIME;
		eeprom_data_read_write(PIR_LOW_ACTIVITY_INFO(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint16_t));
		def_vals = DFLT_MED_ACT_TIME;
		eeprom_data_read_write(PIR_MEDIUM_ACTIVITY_INFO(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint16_t));
		def_vals = DFLT_HIGH_ACT_TIME;
		eeprom_data_read_write(PIR_HIGH_ACTIVITY_INFO(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint16_t));
		def_vals = DFLT_NULL_CNT_WNDWS;
		eeprom_data_read_write(PIR_NULL_CNT_WINDOW(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint8_t));
		def_vals = DFLT_DEC_PRCNTG;
		eeprom_data_read_write(PIR_DEC_PRCNT(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint8_t));
	}

	def.slave_add = 2;
	def.port = 64; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(0,0), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 65; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(0,1), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 64; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(1,0), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 65; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(1,1), WRITE_OP, (uint8_t *)&def, sizeof (def));

//	def.slave_add = 2;
//	def.port = 65; 
//	def.output_port_type = OUTPUT_PORT_TYPE_DO_PULSE;
//	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(2,0), WRITE_OP, (uint8_t *)&def, sizeof (def));
}
#endif

#ifdef TWO_PIR_TOSHIBA_VER3
void set_default (void) {
	struct memory_port_info def;
	uint8_t input_config[] = {INPUT_PORT_TYPE_PIR, INPUT_PORT_TYPE_ON_OFF, INPUT_PORT_TYPE_FEEDBACK, INPUT_PORT_TYPE_PIR};
	uint8_t num_of_op_atchd [] = {2, 3, 1, 2};
	uint8_t input_binding_keys [] =	{1, 1, 1, 1};
	uint8_t idx;
	uint16_t def_vals;

	sys_info.slave_add = 0xFF;
	eeprom_data_read_write(0, WRITE_OP, (uint8_t *)&sys_info.slave_add, 1);

	eeprom_data_read_write(INPUT_CONFIG(0), WRITE_OP, input_config, sizeof (input_config));
	eeprom_data_read_write(OUTPUTS_ATTACHED(0), WRITE_OP, num_of_op_atchd, sizeof (num_of_op_atchd));
	eeprom_data_read_write(INPUT_BINDING(0), WRITE_OP, input_binding_keys, sizeof (input_binding_keys));

	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		def_vals = DFLT_LOW_ACT_TIME;
		eeprom_data_read_write(PIR_LOW_ACTIVITY_INFO(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint16_t));
		def_vals = DFLT_MED_ACT_TIME;
		eeprom_data_read_write(PIR_MEDIUM_ACTIVITY_INFO(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint16_t));
		def_vals = DFLT_HIGH_ACT_TIME;
		eeprom_data_read_write(PIR_HIGH_ACTIVITY_INFO(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint16_t));
		def_vals = DFLT_NULL_CNT_WNDWS;
		eeprom_data_read_write(PIR_NULL_CNT_WINDOW(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint8_t));
		def_vals = DFLT_DEC_PRCNTG;
		eeprom_data_read_write(PIR_DEC_PRCNT(idx), WRITE_OP, (uint8_t *)&def_vals, sizeof (uint8_t));
	}

	def.slave_add = 2;
	def.port = 64; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(0,0), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 65; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO_PULSE;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(0,1), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 64; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(1,0), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 65; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO_PULSE;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(1,1), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 66; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(1,2), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 65; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO_PULSE;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(2,0), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 66; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(3,0), WRITE_OP, (uint8_t *)&def, sizeof (def));

	def.slave_add = 2;
	def.port = 65; 
	def.output_port_type = OUTPUT_PORT_TYPE_DO_PULSE;
	eeprom_data_read_write(INPUT_MAPPED_PORTS_INFO(3,1), WRITE_OP, (uint8_t *)&def, sizeof (def));
}
#endif
/*******************************************************************************************************************************/
//void create_port_pool (uint8_t nodes) {
//	struct port_param *nu_port, *traverse = op_port_mem_pool_head;
//	uint8_t idx;
//
//	for (idx = 0; idx < nodes; idx++) {
//		nu_port = (struct port_param *)malloc (sizeof (struct port_param));
//		if (!nu_port) {
//			DO5_ON;
//		}
//		nu_port->next_port = NULL;
//		/* Check if Port Pool is Empty. */
//		if (op_port_mem_pool_head == NULL) {
//			op_port_mem_pool_head =	nu_port;
//		}
//		else {
//			traverse = op_port_mem_pool_head;
//			/* Load the new port at the end of output port pool. */
//			while (traverse->next_port) {
//				traverse = traverse->next_port;
//			}
//			traverse->next_port = nu_port;
//		}
//	}
//}

void free_port_params (void) {
	struct port_param *del = op_port_mem_pool_head;
	while (del) {
		op_port_mem_pool_head = op_port_mem_pool_head -> next_port;
		free(del);
		del = op_port_mem_pool_head;
	}
}

void free_pir_params (void) {
	uint8_t idx;
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		free (inps [idx].pir_info);
		inps [idx].pir_info = NULL;
	}
}

void free_port_pool (struct ports_pool *port_pool) {
	struct ports_pool *del = port_pool;
	while (del) {
		port_pool = port_pool -> next_port_struct;
		free(del);
		del = port_pool;
	}
}

void free_attached_port_pools (void) {
	uint8_t idx;
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		free_port_pool (inps [idx].port_pool);
		inps [idx].port_pool = NULL;
	}
}

//void create_port_sockets (uint8_t nodes) {
//	struct ports_pool *nu_port, *traverse = inps [0].port_pool;
//	uint8_t idx;
//
//	for (idx = 0; idx < nodes; idx++) {
//		nu_port = (struct ports_pool *)malloc (sizeof (struct ports_pool));
//		if (!nu_port) {
//			DO5_ON;
//		}
//		nu_port->next_port_struct = NULL;
//		/* Check if Port Pool is Empty. */
//		if (inps [0].port_pool == NULL) {
//			inps [0].port_pool = nu_port;
//		}
//		else {
//			traverse = inps [0].port_pool;
//			/* Load the new port at the end of output port pool. */
//			while (traverse->next_port_struct) {
//				traverse = traverse->next_port_struct;
//			}
//			traverse->next_port_struct = nu_port;
//		}
//	}
//}
//
//void free_port_sockets (void) {
//	struct ports_pool *del = inps [0].port_pool;
//	while (del) {
//		inps [0].port_pool = inps [0].port_pool->next_port_struct;
//		free(del);
//		del = inps [0].port_pool;
//	}
//}
//
//void create_pir_params (void) {
//	struct pir_params *nu_port;
//	uint8_t idx;
//
//	for (idx = 0; idx < 4; idx++) {
//		nu_port = (struct pir_params *)malloc (sizeof (struct pir_params));
//		if (!nu_port) {
//			DO5_ON;
//		}
//		inps [idx].pir_info = nu_port;
//	}
//}
//
