/************************************************************************//**
* @file			schedule.c
*
* @brief		Contains function for schedule
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 03/06/14 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n  
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
/*
**===========================================================================
**		Include section
**===========================================================================
*/
#include<stm32f0xx.h>
//#include <RTL.h>
#include <string.h>
//#include "misc.h"
//#include "uart.h"
//#include "ethernet_packet.h"
#include "activateModules.h"
#include "automated_tasks.h"
#include "client_registration.h"
#include "target.h"
#include "water_management.h"
#include "buzzer.h"
#include "bitops.h"
//#include "sensor.h"
//#include "AT45DB161D.h"
//#include "spi.h"
//#include "logs.h"
#include "schedule.h"
//#include "adc.h"
#include "rtc.h"
#include "eeprom.h"


/*
**===========================================================================
**		Defines section
**===========================================================================
*/


extern RTC_TimeTypeDef RTC_TimeStructure;
extern RTC_DateTypeDef date_obj, RTC_DateStruct;
extern uint16_t day_time_in_minutes_glbl;
extern struct tank_info OHT_tank, UGT_tank;
extern struct wms_packet gui_send;
extern struct loc_tank_trigger_flags trigger_state;
extern uint8_t error_flag_num, oht_pump_trigger_cause , ugt_pump_trigger_cause;
extern schedule_window oht_schedule_window [TOTAL_SCHEDULE], buzzer_schedule_window [TOTAL_SCHEDULE];    //, ugt_schedule_window [TOTAL_SCHEDULE]
extern tank_schedule oht_schd, ugt_schd, buzzer_schd;
extern volatile uint8_t  oht_feedback_volt, ugt_feedback_volt, power_pin_current_status, schedule_enable_flag;;
extern uint8_t json_send_arr[1000], *json_send_ptr, dry_run_flag_schedule, dry_run_count_schedule;

uint8_t *error_code = "{\"sid\":\"9000\",\"sad\":\"x\",\"err\":\"10\"}";
/********************************************************************************************************************************
 * Function name: 	void configure_alarm(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	23 Jan, 2015
 *
 * Date modified:	None
 *
 * Description: 	This function is used to read the saved alarm information and populate schedules accordingly.
 *
 * Notes:
 *******************************************************************************************************************************/
void read_schedules (uint8_t tank) {
	schedule_mem_struct read_schd;
	uint8_t idx;
	uint32_t address, read_addr;
	schedule_window *schedule_window_ptr;
	tank_schedule *tank_schedule_ptr;

	if ((tank >= 1) && (tank <= 25)) {
		address = OHT_ALARMS_OFFSET;
		schedule_window_ptr = oht_schedule_window;
		oht_schd.schd_running_flag = 0;
		tank_schedule_ptr = &oht_schd;
	}
//	else if ((tank >= 26) && (tank <= 50)) {
//		address = UGT_ALARMS_OFFSET;
//		schedule_window_ptr = ugt_schedule_window;
//		ugt_schd.schd_running_flag = 0;
//		tank_schedule_ptr = &ugt_schd;
//	}
	else if (tank == 51) {
		address = BUZZER_ALARMS_OFFSET;
		schedule_window_ptr = buzzer_schedule_window;
		buzzer_schd.schd_running_flag = 0;
		tank_schedule_ptr = &buzzer_schd;
	}
	tank_schedule_ptr->active_schedules = 0;
	for (idx = 0; idx < TOTAL_SCHEDULE; idx++) {
		// Readig Alarms and filling appropriate buffers.
		read_addr = (address + (sizeof (schedule_mem_struct) * idx)); 
	//	dflash_read_multiple_byte(read_addr, (uint8_t *)&read_schd, sizeof (read_schd));
		eeprom_data_read_write(read_addr, READ_OP,(uint8_t *)&read_schd, sizeof (read_schd));
		schedule_window_ptr[idx].schd_start.schedule_mins = read_schd.start_mins;
		schedule_window_ptr[idx].schedule_state = read_schd.schedule_stat;
		schedule_window_ptr[idx].schd_end.schedule_mins = read_schd.stop_mins;
		schedule_window_ptr[idx].schedule_type = read_schd.schedule_type;
		schedule_window_ptr[idx].all_schedule_days = read_schd.day;

		if (read_schd.schedule_stat == SCHEDULE_ENABLE) {
			tank_schedule_ptr->active_schedules++;
		}
	}

	if ((tank >= 1) && (tank <= 25)) {
		schedule_window_ptr = oht_schedule_window;
	}
//	else if ((tank >= 26) && (tank <= 50)) {
//		schedule_window_ptr = ugt_schedule_window;
//	}
	else if (tank == 51) {
		schedule_window_ptr = buzzer_schedule_window;
	}
	
	configure_alarm (tank_schedule_ptr, schedule_window_ptr);
}

uint8_t check_schedule_window_active (tank_schedule *tank_schedule_ptr) {
//	return ((day_time_in_minutes_glbl < tank_schedule_ptr->curr_schd.schd_end.schedule_mins) && (day_time_in_minutes_glbl >= tank_schedule_ptr->curr_schd.schd_start.schedule_mins) && (tank_schedule_ptr->curr_schd.schedule_day == date_obj.RTC_WeekDay)? 1 : 0);
	uint8_t ret = 0;

	if((day_time_in_minutes_glbl < tank_schedule_ptr->curr_schd.schd_end.schedule_mins) && (day_time_in_minutes_glbl == tank_schedule_ptr->curr_schd.schd_start.schedule_mins) && (tank_schedule_ptr->curr_schd.schedule_day == date_obj.RTC_WeekDay) == 1) {
		dry_run_flag_schedule = 0;
		dry_run_count_schedule = 0;
		ret =  1;
	}
	else if((day_time_in_minutes_glbl < tank_schedule_ptr->curr_schd.schd_end.schedule_mins) && (day_time_in_minutes_glbl > tank_schedule_ptr->curr_schd.schd_start.schedule_mins) && (tank_schedule_ptr->curr_schd.schedule_day == date_obj.RTC_WeekDay) == 1) {
	  ret =  2;
  }
	else
		ret =  0;

	return ret;
}



uint8_t chk_exception_schedule(uint8_t tank_num){
	uint8_t ret_status = 0;
		
	if(tank_num == 1){
		if((day_time_in_minutes_glbl >= oht_schd.curr_schd.schd_start.schedule_mins) && day_time_in_minutes_glbl <= oht_schd.curr_schd.schd_end.schedule_mins) {
			if (oht_schd.curr_schd.schedule_type){
				ret_status = 1;
			}
				//ret_status 
		}				
	}
	else if(tank_num == 26){
		if((day_time_in_minutes_glbl >= ugt_schd.curr_schd.schd_start.schedule_mins) && day_time_in_minutes_glbl <= ugt_schd.curr_schd.schd_end.schedule_mins) {
			if (ugt_schd.curr_schd.schedule_type){
				ret_status = 1;
			}
				//ret_status 
		}				
	}
	else if(tank_num == 51){
		if((day_time_in_minutes_glbl >= buzzer_schd.curr_schd.schd_start.schedule_mins) && day_time_in_minutes_glbl <= buzzer_schd.curr_schd.schd_end.schedule_mins) {
			if (!buzzer_schd.curr_schd.schedule_type){
				ret_status = 1;
			}
				//ret_status 
		}				
	}
	return ret_status;
}

/********************************************************************************************************************************
 * Function name: 	void save_schedules (struct json_struct *wms_payload_info)
 *
 * Returns: 			0 - on successfully save/disable/delete 
 *						19 - on failure save
 *						20-  on failure disable
 *						21-  on failure delete	
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	23 Jan, 2015
 *
 * Date modified:	None
 *
 * Description: 	This function is used to save alarm information as received form JSON Packet.
 *
 * Notes:
 *******************************************************************************************************************************/

uint8_t save_schedules (struct json_struct *wms_payload_info) {
	volatile uint8_t id = 0;
	uint8_t payload_field_len, payload_loc = 0, field_arr [14], tank_num, stat, ret_stat = 6, idx;
	uint16_t time;
	schedule_mem_struct save_schd, read_schd;
	uint32_t read_addr;

	payload_field_len = extract_json_field_val(field_arr, &wms_payload_info->data[payload_loc]);
	payload_loc += (payload_field_len + 2);									// extract " "
	tank_num = ascii_decimal(field_arr, payload_field_len);	

	payload_field_len = extract_json_field_val(field_arr, &wms_payload_info->data[payload_loc]);
	payload_loc += (payload_field_len + 2);									// extract " "
	id = ascii_decimal(field_arr, payload_field_len);	// schd_id	

	payload_field_len = extract_json_field_val(field_arr, &wms_payload_info->data[payload_loc]);
	payload_loc += (payload_field_len + 2);									// extract " "
	stat = ascii_decimal(field_arr, payload_field_len);		// schd_stat

	if (stat == SCHEDULE_ENABLE){
		save_schd.schedule_stat = stat;
		payload_field_len = extract_json_field_val(field_arr, &wms_payload_info->data[payload_loc]);
		payload_loc += (payload_field_len + 2);								// extract " "
		save_schd.schedule_type = ascii_decimal(field_arr, payload_field_len);	// schd_type
	
		payload_field_len = extract_json_field_val(field_arr, &wms_payload_info->data[payload_loc]);
		payload_loc += (payload_field_len + 2);								// extract " "
		time = ascii_decimal(field_arr, payload_field_len);	// start_time
		save_schd.start_mins = time;

		payload_field_len = extract_json_field_val(field_arr, &wms_payload_info->data[payload_loc]);
		payload_loc += (payload_field_len + 2);								// extract " "
		time = ascii_decimal(field_arr, payload_field_len);	// start_time
		save_schd.stop_mins = time;
	
		payload_field_len = extract_json_field_val(field_arr, &wms_payload_info->data[payload_loc]);
		payload_loc += (payload_field_len + 2);								// extract " "
		save_schd.day = ascii_decimal(field_arr, payload_field_len);		// day

		if ((tank_num >= 1) && (tank_num <= 25))  {
			if( (id == 0) && (oht_schd.active_schedules < TOTAL_SCHEDULE)){
				for (idx = 0; idx < TOTAL_SCHEDULE; idx++) {
					// Readig Alarms and filling appropriate buffers.
					#ifdef FW_VER_GREATER_THAN_3_7_25 
						read_addr = (NEW_OHT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * idx)); 
					#else
						read_addr = (OHT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * idx)); 
					#endif
					//dflash_read_multiple_byte(read_addr, (uint8_t *)&read_schd, sizeof (read_schd));
					eeprom_data_read_write(read_addr, READ_OP,(uint8_t *)&read_schd, sizeof (read_schd));
					if(read_schd.schedule_stat == SCHEDULE_DELETE){
						#ifdef FW_VER_GREATER_THAN_3_7_25 
						//	dflash_write_multiple_byte(NEW_OHT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
						eeprom_data_read_write(NEW_OHT_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd)); 						
						#else
						//	dflash_write_multiple_byte(OHT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd)); 
						eeprom_data_read_write(OHT_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
						#endif
						ret_stat = idx + 1;
						break;
					}
				}
			}
			else if(id!= 0 && (oht_schd.active_schedules <= TOTAL_SCHEDULE)){
				#ifdef FW_VER_GREATER_THAN_3_7_25 
				//	dflash_write_multiple_byte(NEW_OHT_ALARM_ADDR ((id-1)), (uint8_t *)&save_schd, sizeof (save_schd)); 
					eeprom_data_read_write(NEW_OHT_ALARM_ADDR ((id-1)), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));						
				#else
					//dflash_write_multiple_byte(OHT_ALARM_ADDR ((id-1)), (uint8_t *)&save_schd, sizeof (save_schd));
					eeprom_data_read_write(OHT_ALARM_ADDR ((id-1)), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd)); 
				#endif
				ret_stat = id;
			}
			if(ret_stat > TOTAL_SCHEDULE){
				ret_stat = SCHEDULE_SAVE_ERROR;	
			}
		}
			
		else if ((tank_num >= 26) && (tank_num <= 50))  {
			if(id == 0 && (ugt_schd.active_schedules < TOTAL_SCHEDULE)){
				for (idx = 0; idx < TOTAL_SCHEDULE; idx++) {
					// Readig Alarms and filling appropriate buffers.
					#ifdef FW_VER_GREATER_THAN_3_7_25 
						read_addr = (NEW_UGT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * idx)); 
					#else
						read_addr = (UGT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * idx)); 
					#endif
					//read_addr = (UGT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * idx)); 
					//dflash_read_multiple_byte(read_addr, (uint8_t *)&read_schd, sizeof (read_schd));
					eeprom_data_read_write(read_addr, READ_OP,(uint8_t *)&read_schd, sizeof (read_schd));
					if(read_schd.schedule_stat == SCHEDULE_DELETE){
						#ifdef FW_VER_GREATER_THAN_3_7_25 
							//dflash_write_multiple_byte(NEW_UGT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
							eeprom_data_read_write(NEW_UGT_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd)); 						
						#else
							//dflash_write_multiple_byte(UGT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
							eeprom_data_read_write(UGT_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd)); 
						#endif
						//dflash_write_multiple_byte(UGT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd)); 
						ret_stat = idx + 1;
						break;
					}
				}
			}
			else if(id!= 0 && (ugt_schd.active_schedules <= TOTAL_SCHEDULE)){
				#ifdef FW_VER_GREATER_THAN_3_7_25 
					//dflash_write_multiple_byte(NEW_UGT_ALARM_ADDR ((id-1)), (uint8_t *)&save_schd, sizeof (save_schd));
					eeprom_data_read_write(NEW_UGT_ALARM_ADDR ((id-1)), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd)); 						
				#else
					//dflash_write_multiple_byte(UGT_ALARM_ADDR ((id-1)), (uint8_t *)&save_schd, sizeof (save_schd));
					eeprom_data_read_write(UGT_ALARM_ADDR ((id-1)), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd)); 
				#endif
			//	dflash_write_multiple_byte(UGT_ALARM_ADDR ((id-1)), (uint8_t *)&save_schd, sizeof (save_schd)); 
				ret_stat = id;
			}
			if(ret_stat == 6){
				ret_stat = SCHEDULE_SAVE_ERROR;	
			}
		}
		else if (tank_num == 51 ) {
			if(id == 0 && (buzzer_schd.active_schedules < TOTAL_SCHEDULE)){
				for (idx = 0; idx < TOTAL_SCHEDULE; idx++) {
					// Readig Alarms and filling appropriate buffers.
					#ifdef FW_VER_GREATER_THAN_3_7_25 
						read_addr = (NEW_BUZZER_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * idx)); 
					#else
						read_addr = (BUZZER_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * idx)); 
					#endif
					//read_addr = (BUZZER_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * idx)); 
					//dflash_read_multiple_byte(read_addr, (uint8_t *)&read_schd, sizeof (read_schd));
					eeprom_data_read_write(read_addr, READ_OP,(uint8_t *)&read_schd, sizeof (read_schd));
					if(read_schd.schedule_stat == SCHEDULE_DELETE){
						#ifdef FW_VER_GREATER_THAN_3_7_25 
						//	dflash_write_multiple_byte(NEW_BUZZER_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
							eeprom_data_read_write(NEW_BUZZER_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd)); 						
						#else
						//	dflash_write_multiple_byte(BUZZER_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd)); 
							eeprom_data_read_write(BUZZER_ALARM_ADDR (idx), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));
						#endif
						
						//dflash_write_multiple_byte(BUZZER_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd)); 
						ret_stat = idx + 1;
						break;
					}
				}
			}
			else if(id!= 0 && (buzzer_schd.active_schedules <= TOTAL_SCHEDULE)){
				#ifdef FW_VER_GREATER_THAN_3_7_25 
					//dflash_write_multiple_byte(NEW_BUZZER_ALARM_ADDR ((id - 1)), (uint8_t *)&save_schd, sizeof (save_schd)); 
					eeprom_data_read_write(NEW_BUZZER_ALARM_ADDR ((id - 1)), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));						
				#else
					//dflash_write_multiple_byte(BUZZER_ALARM_ADDR ((id - 1)), (uint8_t *)&save_schd, sizeof (save_schd));
					eeprom_data_read_write(BUZZER_ALARM_ADDR ((id - 1)), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd)); 
				#endif
				
				//dflash_write_multiple_byte(BUZZER_ALARM_ADDR ((id - 1)), (uint8_t *)&save_schd, sizeof (save_schd)); 
				ret_stat = id;
			
			}
			if(ret_stat == 6){
				ret_stat = SCHEDULE_SAVE_ERROR;	
			}
		}
		else{
			ret_stat = SCHEDULE_SAVE_ERROR; // error 
		}
		//}
	}
	else if ((stat == SCHEDULE_DISABLE) || (stat == SCHEDULE_DELETE)) {
		if (id <= TOTAL_SCHEDULE) {
			if ((tank_num >= 1) && (tank_num <= 25)) {
				id--;
				#ifdef FW_VER_GREATER_THAN_3_7_25 
				//	dflash_read_multiple_byte(NEW_OHT_ALARM_ADDR ((id)), (uint8_t *)&save_schd, sizeof (save_schd)); 
					eeprom_data_read_write(NEW_OHT_ALARM_ADDR ((id)), READ_OP, (uint8_t *)&save_schd, sizeof (save_schd));						
				#else
				//	dflash_read_multiple_byte(OHT_ALARM_ADDR ((id)), (uint8_t *)&save_schd, sizeof (save_schd));
					eeprom_data_read_write(OHT_ALARM_ADDR ((id)), READ_OP, (uint8_t *)&save_schd, sizeof (save_schd)); 
				#endif
			//	dflash_read_multiple_byte(OHT_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
				save_schd.schedule_stat = stat;
				#ifdef FW_VER_GREATER_THAN_3_7_25 
				//	dflash_write_multiple_byte(NEW_OHT_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
					eeprom_data_read_write(NEW_OHT_ALARM_ADDR (id), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd)); 						
				#else
				//	dflash_write_multiple_byte(OHT_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd)); 
					eeprom_data_read_write(OHT_ALARM_ADDR (id), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));
				#endif

			//	dflash_write_multiple_byte(OHT_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
				ret_stat = 0;
			}
			else if ((tank_num >= 26) && (tank_num <= 50)) {
				id--;
				#ifdef FW_VER_GREATER_THAN_3_7_25 
				//	dflash_read_multiple_byte(NEW_UGT_ALARM_ADDR ((id)), (uint8_t *)&save_schd, sizeof (save_schd)); 
					eeprom_data_read_write(NEW_UGT_ALARM_ADDR ((id)), READ_OP, (uint8_t *)&save_schd, sizeof (save_schd));						
				#else
				//	dflash_read_multiple_byte(UGT_ALARM_ADDR ((id)), (uint8_t *)&save_schd, sizeof (save_schd)); 
					eeprom_data_read_write(UGT_ALARM_ADDR ((id)), READ_OP,(uint8_t *)&save_schd, sizeof (save_schd));
				#endif
			//	dflash_read_multiple_byte(OHT_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
				save_schd.schedule_stat = stat;
				#ifdef FW_VER_GREATER_THAN_3_7_25 
				//	dflash_write_multiple_byte(NEW_UGT_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd)); 
					eeprom_data_read_write(NEW_UGT_ALARM_ADDR (id), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));						
				#else
				//	dflash_write_multiple_byte(UGT_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
					eeprom_data_read_write(UGT_ALARM_ADDR (id), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd)); 
				#endif

//				dflash_read_multiple_byte(UGT_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
//				save_schd.schedule_stat = stat;
//				dflash_write_multiple_byte(UGT_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
				ret_stat = 0;
			}
			else if (tank_num == 51) {
				id--;
				#ifdef FW_VER_GREATER_THAN_3_7_25 
				//	dflash_read_multiple_byte(NEW_BUZZER_ALARM_ADDR ((id)), (uint8_t *)&save_schd, sizeof (save_schd));
					eeprom_data_read_write(NEW_BUZZER_ALARM_ADDR ((id)), READ_OP,(uint8_t *)&save_schd, sizeof (save_schd)); 						
				#else
				//	dflash_read_multiple_byte(BUZZER_ALARM_ADDR ((id)), (uint8_t *)&save_schd, sizeof (save_schd));
					eeprom_data_read_write(BUZZER_ALARM_ADDR ((id)), READ_OP,(uint8_t *)&save_schd, sizeof (save_schd)); 
				#endif
			//	dflash_read_multiple_byte(OHT_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
				save_schd.schedule_stat = stat;
				#ifdef FW_VER_GREATER_THAN_3_7_25 
				//	dflash_write_multiple_byte(NEW_BUZZER_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
					eeprom_data_read_write(NEW_BUZZER_ALARM_ADDR (id), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd)); 						
				#else
				//	dflash_write_multiple_byte(BUZZER_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd)); 
					eeprom_data_read_write(BUZZER_ALARM_ADDR (id), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));
				#endif
//				dflash_read_multiple_byte(BUZZER_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
//				save_schd.schedule_stat = stat;
//				dflash_write_multiple_byte(BUZZER_ALARM_ADDR (id), (uint8_t *)&save_schd, sizeof (save_schd));
//				ret_stat = 0;
			}
			else{
				if(stat == SCHEDULE_DISABLE)
					ret_stat = SCHEDULE_DISABLE_ERROR; // error 	

				else if(stat == SCHEDULE_DELETE)
					ret_stat = SCHEDULE_DELETE_ERROR; // error 		
			}
			
		}
	}
//	if(schedule_enable_flag == 1)
//		default_schedule_fetch();
//	else
		read_schedules (OHT_tank.tank_config_ptr->tank_num);
	//read_schedules (tank_num);
	return ret_stat;
}








/********************************************************************************************************************************
 * Function name: 	void configure_alarm(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	23 Jan, 2015
 *
 * Date modified:	None
 *
 * Description: 	This function is used to read the saved alarm information and populate schedules accordingly.
 *
 * Notes:
 *******************************************************************************************************************************/
void default_schedule_fetch (void) {
	
	oht_schd.schd_running_flag = 0;
	oht_schd.active_schedules = 1;
	oht_schd.curr_schd.all_schedule_days = 1234567;
	oht_schd.curr_schd.schd_start.schedule_mins = 1;
	oht_schd.curr_schd.schd_end.schedule_mins = 1440;
	oht_schd.curr_schd.schedule_day = date_obj.RTC_WeekDay;
	oht_schd.curr_schd.schedule_state = 1;
	oht_schd.curr_schd.schedule_type = 0;

}







uint8_t getday_for_next_schedule (uint32_t schedule_days) {
	uint32_t all_schedule_days = schedule_days;
	uint32_t day = all_schedule_days;

	while (all_schedule_days) {
		day = all_schedule_days % 10;
		if (day == date_obj.RTC_WeekDay) {
			return day;
		}
		all_schedule_days /= 10;
	}
	return day;
}

/********************************************************************************************************************************
 * Function name: 	void sort_alarm_buffer(alarm_time *tank_ptr, uint8_t no_of_alarms)
 *
 * Returns: 		None
 *
 * Arguments: 		alarm_time				*tank_ptr
 * 					uint8_t					no_of_alarms
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	2 Nov, 2013
 *
 * Date modified:	None
 *
 * Description: 	This function is used to sort all alarm schedules for both the tanks in ascending order.
 *
 * Notes:
 *******************************************************************************************************************************/
void sort_alarm_buffer(schedule_window *schd_ptr, uint8_t num_of_alarms){
	uint8_t idx = 0, ele;
	schedule_window sort_obj;

	/* Bubble Sorting according to active alarms. */
	for(ele = 0; ele < TOTAL_SCHEDULE; ele++){
		for(idx = 0; idx < (TOTAL_SCHEDULE - ele - 1); idx++){
			if(schd_ptr[idx].schedule_state != SCHEDULE_ENABLE){
				sort_obj = schd_ptr[idx];
				schd_ptr[idx] = schd_ptr[idx + 1];
				schd_ptr[idx + 1] = sort_obj;
			}
		}
	}

	/* Bubble Sorting according to alarm times. */
	for(ele = 0; ele < num_of_alarms; ele++){
		/* when is the next schedule. */
		schd_ptr [ele].schedule_day = getday_for_next_schedule (schd_ptr [ele].all_schedule_days);
	}
	for(ele = 0; ele < num_of_alarms; ele++){
		for(idx = 0;idx < (num_of_alarms-ele-1); idx++) {
			if(schd_ptr[idx].schd_end.schedule_mins > schd_ptr[idx+1].schd_end.schedule_mins){
				sort_obj = schd_ptr[idx];
				schd_ptr[idx] = schd_ptr[idx + 1];
				schd_ptr[idx + 1] = sort_obj;
			}
		}
	}
}

/********************************************************************************************************************************
 * Function name: 	void configure_alarm(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	2 Nov, 2013
 *
 * Date modified:	None
 *
 * Description: 	This function is used to configure next alarm event and the motor action to be taken for both tanks.
 *
 * Notes:
 *******************************************************************************************************************************/
void configure_alarm(tank_schedule *schd, schedule_window *window){
	uint8_t idx;
	schedule_window *temp_window;
	
	temp_window =window; 
	sort_alarm_buffer(window, schd->active_schedules);
	schd->curr_schd.schd_start.schedule_mins = schd->curr_schd.schd_end.schedule_mins = 0xFFFF;

	window =temp_window; 
	/* Updating next Tank Alarm & Action (ON/OFF). */
	for(idx = 0; idx < schd->active_schedules; idx++){
		if (window [idx].schedule_day == date_obj.RTC_WeekDay) {
			if (day_time_in_minutes_glbl < window [idx].schd_end.schedule_mins) {
				schd->curr_schd = window[idx];
				break;
			}
		}
//		if((window [idx].schedule_day == date_obj.RTC_WeekDay) && (day_time_in_minutes_glbl < window [idx].schd_end.schedule_mins)){
//			/* Next Schedule is today. */
//			schd->curr_schd = window[idx];
//			break;
//		}
//		else {
//			schd->curr_schd = window[0];
//			break;
//		}
	}
}

void save_default_schedules (void) {
	uint8_t idx;
	schedule_mem_struct save_schd;
	save_schd.schedule_stat = SCHEDULE_DELETE;

	for (idx = 0; idx < TOTAL_SCHEDULE; idx++) {
		#ifdef FW_VER_GREATER_THAN_3_7_25
			//dflash_write_multiple_byte(NEW_OHT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(NEW_OHT_ALARM_ADDR (idx),  WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));
			//dflash_write_multiple_byte(NEW_UGT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(NEW_UGT_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
			//dflash_write_multiple_byte(NEW_BUZZER_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(NEW_BUZZER_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
		#else
			//dflash_write_multiple_byte(OHT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(OHT_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
			//dflash_write_multiple_byte(UGT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(UGT_ALARM_ADDR (idx), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));
			//dflash_write_multiple_byte(BUZZER_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(BUZZER_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
		#endif
	
	}
}

void save_disable_all_schedules (void) {
	uint8_t idx;
	schedule_mem_struct save_schd;
	

	for (idx = 0; idx < TOTAL_SCHEDULE; idx++) {
		#ifdef FW_VER_GREATER_THAN_3_7_25
			//dflash_write_multiple_byte(NEW_OHT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(NEW_OHT_ALARM_ADDR (idx),  READ_OP, (uint8_t *)&save_schd, sizeof (save_schd));
			if(save_schd.schedule_stat == SCHEDULE_ENABLE){
				save_schd.schedule_stat = SCHEDULE_DISABLE;		
				eeprom_data_read_write(NEW_OHT_ALARM_ADDR (idx),  WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));
			}
				//dflash_write_multiple_byte(NEW_UGT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(NEW_UGT_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
			//dflash_write_multiple_byte(NEW_BUZZER_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(NEW_BUZZER_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
		#else
			//dflash_write_multiple_byte(OHT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(OHT_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
			//dflash_write_multiple_byte(UGT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(UGT_ALARM_ADDR (idx), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));
			//dflash_write_multiple_byte(BUZZER_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(BUZZER_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
		#endif
	
	}
}


void save_enable_all_schedules (void) {
	uint8_t idx;
	schedule_mem_struct save_schd;
	//save_schd.schedule_stat = SCHEDULE_ENABLE;

	for (idx = 0; idx < TOTAL_SCHEDULE; idx++) {
		#ifdef FW_VER_GREATER_THAN_3_7_25
			//dflash_write_multiple_byte(NEW_OHT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(NEW_OHT_ALARM_ADDR (idx),  READ_OP, (uint8_t *)&save_schd, sizeof (save_schd));
			if(save_schd.schedule_stat == SCHEDULE_DISABLE){
				save_schd.schedule_stat = SCHEDULE_ENABLE;	
				eeprom_data_read_write(NEW_OHT_ALARM_ADDR (idx),  WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));
			}	
				//dflash_write_multiple_byte(NEW_UGT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
				eeprom_data_read_write(NEW_UGT_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
				//dflash_write_multiple_byte(NEW_BUZZER_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
				eeprom_data_read_write(NEW_BUZZER_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
		#else
			//dflash_write_multiple_byte(OHT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(OHT_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
			//dflash_write_multiple_byte(UGT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(UGT_ALARM_ADDR (idx), WRITE_OP, (uint8_t *)&save_schd, sizeof (save_schd));
			//dflash_write_multiple_byte(BUZZER_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(BUZZER_ALARM_ADDR (idx), WRITE_OP,(uint8_t *)&save_schd, sizeof (save_schd));
		#endif
	
	}
}




uint8_t get_schedule_state (uint8_t disable_enable_flag) {
	uint8_t idx;
	schedule_mem_struct save_schd;
	//save_schd.schedule_stat = SCHEDULE_ENABLE;

	for (idx = 0; idx < TOTAL_SCHEDULE; idx++) {
		#ifdef FW_VER_GREATER_THAN_3_7_25
			//dflash_write_multiple_byte(NEW_OHT_ALARM_ADDR (idx), (uint8_t *)&save_schd, sizeof (save_schd));
			eeprom_data_read_write(NEW_OHT_ALARM_ADDR (idx),  READ_OP, (uint8_t *)&save_schd, sizeof (save_schd));
			if(save_schd.schedule_stat == disable_enable_flag){
						return 1;
			}	
			#endif	
		}
		return 0;
}


void get_tank_schedule(uint8_t tank_number){
 uint8_t loc_flag = 0, field_name_indx = 1, len, count, countr, tank_num, start_brace_flag  =0, *ptr = 0, temp_arr[50];
 schedule_mem_struct schedule_window_ptr;
 
 #ifdef FW_VER_GREATER_THAN_3_7_25
  for(count = 0; count < TOTAL_SCHEDULE; count++){
    if(tank_number >= 1 && tank_number <= 25){
   // dflash_read_multiple_byte(OHT_ALARM_ADDR (count), (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
    eeprom_data_read_write(OHT_ALARM_ADDR (count), READ_OP,(uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
    tank_num = 1;
   }
   else if(tank_number >= 26 && tank_number <= 50){
   // dflash_read_multiple_byte(UGT_ALARM_ADDR (count), (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
    eeprom_data_read_write(UGT_ALARM_ADDR (count), READ_OP, (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
    tank_num = 26;
   }
   else if(tank_number == 51){
  //  dflash_read_multiple_byte(BUZZER_ALARM_ADDR (count), (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
   eeprom_data_read_write(BUZZER_ALARM_ADDR (count), READ_OP, (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
    tank_num = 51;
   }
   /* if schedule is enable/ disable */
   if(schedule_window_ptr.schedule_stat != SCHEDULE_DELETE){
    if(start_brace_flag == 0){
     memcpy(temp_arr, json_send_arr, 44);
     json_send_arr[0] = '{';
     memcpy(&json_send_arr[1], temp_arr, 44);
     start_brace_flag = 1; 
		*json_send_ptr++ = ':';   //atul colon			
						
    }
		
    field_name_indx = 1;
    if(loc_flag == 1){
     ADD_CLOSING_BRACE;
		 ADD_COMMA;
     memcpy(json_send_ptr, &json_send_arr[1], 44);   /* add header part */
     json_send_ptr += 44;
		*json_send_ptr++ = ':';   //atul colon
    }
  //  else
		
    ADD_START_BRACE;
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(tank_num, json_send_ptr);    /* add tank number */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA; 
    
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(count + 1, json_send_ptr);    /* add schedule id */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA;   
 
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(schedule_window_ptr.schedule_stat, json_send_ptr);    /* add schedule state */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA;  
    
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(schedule_window_ptr.schedule_type, json_send_ptr);    /* add schedule_type */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA;   
    
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(schedule_window_ptr.start_mins, json_send_ptr);    /* add start timer */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA;  
    
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(schedule_window_ptr.stop_mins, json_send_ptr);    /* add start timer */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA;      
 
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(schedule_window_ptr.day, json_send_ptr);    /* add start timer */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_CLOSING_BRACE;
    loc_flag = 1; 
		
    }
  }

		if(start_brace_flag == 1) {
			ADD_CLOSING_BRACE;		
		}

	
 #else
 /* scan oht schedule */    
 for(countr = 0; countr < 3; countr++){
  for(count = 0; count < TOTAL_SCHEDULE; count++){
    if(countr == 0){
  //  dflash_read_multiple_byte(OHT_ALARM_ADDR (count), (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
   	  eeprom_data_read_write(OHT_ALARM_ADDR (count), READ_OP, (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
    tank_num = 1;
   }
   else if(countr == 1){
  //  dflash_read_multiple_byte(UGT_ALARM_ADDR (count), (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
	eeprom_data_read_write(UGT_ALARM_ADDR (count), READ_OP, (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
    tank_num = 26;
   }
   else if(countr == 2){
  //  dflash_read_multiple_byte(BUZZER_ALARM_ADDR (count), (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
	eeprom_data_read_write(BUZZER_ALARM_ADDR (count), READ_OP, (uint8_t *)&schedule_window_ptr, sizeof (schedule_window_ptr));
    tank_num = 51;
   }
   /* if schedule is enable/ disable */
   if(schedule_window_ptr.schedule_stat != SCHEDULE_DELETE){
    field_name_indx = 1;
    if(loc_flag == 1){
     ADD_CLOSING_BRACE;
     memcpy(json_send_ptr, json_send_arr, 44);   /* add header part */
     json_send_ptr += 44; 
    }
    ADD_START_BRACE;
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(tank_num, json_send_ptr);    /* add tank number */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA; 
    
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(count + 1, json_send_ptr);    /* add schedule id */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA;   
 
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(schedule_window_ptr.schedule_stat, json_send_ptr);    /* add schedule state */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA;  
    
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(schedule_window_ptr.schedule_type, json_send_ptr);    /* add schedule_type */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA;   
    
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(schedule_window_ptr.start_mins, json_send_ptr);    /* add start timer */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA;  
    
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(schedule_window_ptr.stop_mins, json_send_ptr);    /* add start timer */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_COMMA;      
 
    add_field_name(field_name_indx++);   
    ADD_DOUBLE_QUOTE;
    len = dec_ascii_arr(schedule_window_ptr.day, json_send_ptr);    /* add start timer */
    json_send_ptr += len; 
    ADD_DOUBLE_QUOTE;
    ADD_CLOSING_BRACE;
    loc_flag = 1;  
    }
  } 
 }
 #endif
 /* if no data found */
 if(loc_flag == 0){       /* if no data found then send error */
  json_send_ptr = &json_send_arr[0];
  strncpy((char *)json_send_ptr, (char *)error_code, 31);
  json_send_ptr += 31;
  len = dec_ascii_arr(NO_DATA_FOUND, json_send_ptr);
  json_send_ptr += len; 
  ADD_DOUBLE_QUOTE; 
 }
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/

