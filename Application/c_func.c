/************************************************************************//**
* @file			c_func.c
*
* @brief		Contains  the API for packet validation from GUI
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 5/2/13 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
#include <stm32f0xx.h>
#include "c_func.h"


/*
**===========================================================================
**		Function Prototype Section
**===========================================================================
*/



/************************************************************************//**
*					ascii_dec(char nob)
*
* @brief			This routine converts the ascii bytes to long integer
*
* @param 			nob  - number of bytes to be converted.
*
* @returns			None.
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/02/13
* @note				None.
****************************************************************************/
//long ascii_dec(uint8_t nob){
////	uint8_t loop;
////	long cnvrt_val = 0;
////
////	for(loop = 0 ; loop < nob ; loop++){
////		if(ethernet.RxGetPtr >= ethernet.InBuf + ETH_RX_BUF_LEN){
////			ethernet.RxGetPtr = ethernet.InBuf;
////		}
////		cnvrt_val = (cnvrt_val * 10) + (*(ethernet.RxGetPtr++) - 48);
////	}
////	 return cnvrt_val;
//}

/************************************************************************//**
*					dec_ascii_byte(uint32_t long_value, uint8_t *convrt_ptr, uint8_t nob)
*
* @brief			This routine converts the decimal value to ascii bytes
*
* @param 			nob  - tells the number of bytes to be converted.
*
*					long_value - to be converted into decimal ascii bytes
*
*					*convrt_ptr - pointer to buffer that contains data bytes to be converted
*
* @returns			None.
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				08/02/13
* @note				None.
****************************************************************************/
void dec_ascii_byte(uint32_t long_value, uint8_t *convrt_ptr, uint8_t nob){

	while(nob > 0){
		*(convrt_ptr + nob - 1) = (long_value % 10) + 0x30;
		long_value = long_value/ 10;
		nob--;
	}
}

uint8_t dec_ascii_arr(uint32_t long_value, uint8_t *convrt_ptr){
	uint32_t temp_val, nob = 0, temp_arr[50], count;

	temp_val = long_value;
	if(temp_val > 0){
		while(temp_val > 0){
			temp_arr[nob] = (long_value % 10) + 0x30;
			long_value = long_value/ 10;
			temp_val = 	long_value;
			nob++;
		}
		for(count = 0; count < nob; count++){
			*convrt_ptr++ =  temp_arr[nob - count - 1];	
		}
	}
	else{
		*convrt_ptr++ =	0x30;
		nob = 1;
	}
	return nob;
}

/************************************************************************//**
*					long ascii_decimal(uint8_t *convrt_ptr, uint8_t nob)
*
* @brief			This routine is used to convert the ascii bytes to decimal value
*					
*																							  
* @param *convrt_ptr		Pointer to the variable that contains ascii bytes.
* @param nob				No. of data bytes to be converted.
*
* @returns					Converted long decimal value.
*
* @exception				None.
*
* @author					Nikhil kukreja
* @date						06/02/13
* @note						None.                                         
****************************************************************************/
long ascii_decimal(uint8_t *convrt_ptr, uint8_t nob){
	char loop;
	long cnvrt_val = 0;

	for(loop = 0 ; loop < nob ; loop++){
		cnvrt_val = (cnvrt_val * 10) + (*(convrt_ptr++) - 48);
	}
	 return cnvrt_val;

}

/************************************************************************//**
*					long byte_integer_conversion(uint8_t *convrt_ptr, uint8_t nob)
*
* @brief			This routine converts bytes to long variable.
*					
*
* @param *convrt_ptr		Pointer to the variable that contains bytes.
* @param nob				No. of data bytes to be converted.
*
* @returns					Converted long decimal value.
*
* @exception				None.
*
* @author					Nikhil kukreja
* @date						06/02/13
* @note						None.                                         
****************************************************************************/
long byte_integer_conversion(uint8_t *convrt_ptr, uint8_t nob){
	char loop;
	long cnvrt_val = 0;

	for(loop = 0 ; loop < nob ; loop++){
		cnvrt_val = (cnvrt_val * 10) + (*(convrt_ptr++));
	}
	 return cnvrt_val;

}
/************************************************************************//**
*			void integer_byte_conversion(uint32_t long_value, uint8_t *convrt_ptr, uint8_t nob)
*
* @brief			This routine is converts long value to byte.
*					
* @param long_value			value to be converted.
* @param *convrt_ptr		Pointer to the variable that contains bytes.
* @param nob				No. of data bytes to be converted.
*
* @returns					Converted long decimal value.
*
* @exception				None.
*
* @author					Nikhil kukreja
* @date						06/02/13
* @note						None.                                         
****************************************************************************/
void integer_byte_conversion(uint32_t long_value, uint8_t *convrt_ptr, uint8_t nob){
	
	while(nob > 0){
		*(convrt_ptr + nob - 1) = (long_value % 10);
		long_value = long_value/ 10;
		nob--;
	}
}
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/


