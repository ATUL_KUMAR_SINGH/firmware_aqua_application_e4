/************************************************************************//**
* @file			relay_feedback.c
*
* @brief		Contains function for configure adc channel & their interrupt.
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 03/06/14 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n  
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
/*
**===========================================================================
**		Include section
**===========================================================================
*/

#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_adc.h"

#include "adc.h"
#include "water_management.h"

//#include"ethernet_packet.h"
//#include <RTL.h>


uint16_t rtd_converted_data_recvd, converted_data_recvd_adc1,milli_volts_at_feedback;
uint32_t converted_data_OHT_adc1, converted_data_UGT_adc1, converted_data_VSense_adc1, converted_data_BAT_adc1, converted_data_Version_adc1;
uint16_t adc1_counter_OHT_tank, adc1_counter_UGT_tank, adc1_counter_version, adc1_counter_vsense, adc1_counter_bat;
uint32_t  quetient, remaindr, averaged_adc1_value;
uint8_t oht_feedback_flag, vsense_feedback_flag, version_feedback_flag;
uint8_t HwVersion[3] = {0};

uint16_t adc1_counter, adc3_counter;
uint32_t adc_channel_type = 0;
uint16_t rtd_converted_data_avg, feedback_converted_avg_val, lm35_cnvrtd_avg_val = 0, feedback_cnvrtd_voltage;
//extern OS_TID  feedback_tid, tid_Sensor_switch;
extern uint8_t error_flag_num, power_pin_current_status;
extern uint16_t adc_monitoring, adc_evt_num;
//extern struct tank_info OHT_tank, UGT_tank;
extern uint16_t adc_evt_num;
extern struct wms_packet gui_send;
extern struct wms_sys_info wms_sys_config;
extern volatile uint8_t  oht_feedback_volt , ugt_feedback_volt ;

#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif

/************************************************************************//**
*				void ADC_interrupt_config()
*
* @brief		Configures The ADC Interrupts.
*
* @param		
*
* @returns		None
*
* @exception	None.
*
* @author		Uday Prakash
* @date			21/05/12
*
* @note			
****************************************************************************/  
void ADC_interrupt_config(){

  NVIC_InitTypeDef  NVIC_InitStructure;
 
  NVIC_InitStructure.NVIC_IRQChannel = ADC1_COMP_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
}

/************************************************************************//**
*				void ADC_1_Config(void)
*
* @brief		Function Configures	ADC1 and Enable the ADC and its Software Conversion.
*
* @param		
*
* @returns		None
*
* @exception	None.
*
* @author		Uday Prakash
* @date			21/05/12
*
* @note			
****************************************************************************/
void ADC_1_Config(void)
{
 
	GPIO_InitTypeDef GPIO_InitStructure;
	ADC_InitTypeDef ADC_InitStructure;

	/* Put everything back to power-on defaults */
	ADC_DeInit(ADC1);
	  

	
	/* Enable ADC GPIO clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	/* ADC1 Periph clock enable */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

		
	/* Configure ADC1 pin 0 as ADC_FDB_PUMP ************************/
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* Configure ADC1 pin 1 as VSENSE ******************************/
//	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_1;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
//	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
//	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* Configure ADC1 pin 2 as VERSION_CONTROL *********************/
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
		
	/* PCLK is the APB2 clock */
	/* ADCCLK = PCLK/4 = 48/4 = 12MHz*/
	RCC_ADCCLKConfig(RCC_ADCCLK_PCLK_Div4);				
//	RCC_ADCCLKConfig(RCC_ADCCLK_HSI14);





	/* ADC1 Configuration ------------------------------------------------------*/
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;    
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;
	ADC_Init(ADC1, &ADC_InitStructure); 

	
	
	/* ADC-NVIC Enabling */	
	ADC_interrupt_config ();
	
	//ADC_ClockModeConfig(ADC1, ADC_ClockMode_AsynClk);

	/* ADC Calibration */
	ADC_GetCalibrationFactor(ADC1);
	
	/* Enable the auto delay feature */    
	ADC_WaitModeCmd(ADC1, ENABLE); 
	    
}



/************************************************************************//**
* void ADC_1_Config(void)
*
* @brief Function Configures ADC1 and Enable the ADC and its Software Conversion.
*
* @param 
*
* @returns None
*
* @exception None.
*
* @author Uday Prakash
* @date 21/05/12
*
* @note 
****************************************************************************/
void clear_all_channel_adc1() {

ADC1->CHSELR = 0x00000000; //clear all channel

}

 

 

 

/************************************************************************//**
* void ADC_1_Config(void)
*
* @brief Function Configures ADC1 and Enable the ADC and its Software Conversion.
*
* @param 
*
* @returns None
*
* @exception None.
*
* @author Uday Prakash
* @date 21/05/12
*
* @note 
****************************************************************************/
void clear_one_channel_adc1(uint32_t channel) {

ADC1->CHSELR &= ~(channel); //clear selected channel

}




/*! \fn       	void activate_adc3_channel (uint8_t channel)
 *  \breif    	Activate nth channel for ADC 3
 *	\param		channel: channel number that can take value from ADC_Channel_0 to ADC_Channel_18
 *	\return		NONE
 *
 *  \author   	Praveen
 */
void activate_adc1_channel (uint32_t channel) {

adc_channel_type = channel;

clear_all_channel_adc1();

/* ADC regular channel configuration */ 
ADC_ChannelConfig(ADC1, channel, ADC_SampleTime_239_5Cycles );

ADC_AnalogWatchdogSingleChannelConfig(ADC1,channel);

ADC_ITConfig(ADC1, ADC_IT_EOC, ENABLE);

/* Enable ADC1 */
ADC_Cmd(ADC1, ENABLE);

/* Wait the ADCEN falg */
while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADEN));

/* ADC1 regular Software Start Conv */
ADC_StartOfConversion(ADC1);
}


void select_feedback_chnl(void){

	if(adc_channel_type == ADC_Channel_0){
		activate_adc1_channel(ADC_Channel_0);
		while(oht_feedback_flag == 0);
		oht_feedback_flag = 0;
		
	}		
//	if(adc_channel_type == ADC_Channel_1){	
//		activate_adc1_channel(ADC_Channel_1);
//		while(vsense_feedback_flag == 0);
//		vsense_feedback_flag = 0;
//	}
	if(adc_channel_type == ADC_Channel_1){	
		activate_adc1_channel(ADC_Channel_1);
		while(version_feedback_flag == 0);
		version_feedback_flag = 0;
	}

}



uint16_t readADC1(uint32_t channel) {
//	ADC_StopOfConversion (ADC1);
	/* Clear the selected ADC Channel */
	ADC1->CHSELR = 0;
	/* Convert the ADC1 input with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, channel, ADC_SampleTime_55_5Cycles); // PA.1 - IN1	// Start the conversion
	/* ADC1 regular Software Start Conv */
	ADC_StartOfConversion(ADC1);	// Wait until conversion completion
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	// Get the conversion value
	return (((ADC_GetConversionValue(ADC1)) * 3300)/0x03FF);
}



/************************************************************************//**
*				void ADC_IRQHandler(void)
*
* @brief		this function handels the ADC Interrupt and acquires the value.
*
* @param		None
*
* @returns		None
*
* @exception	None.
*
* @author		Aman Deep & Uday Prakash
* @date			21/05/12
*
* @note			ADC1 channel2 used for LM35 & ADC1 channel3 used for RTD
*				ADC3 channel9 used for feedback data
****************************************************************************/  
void ADC1_IRQHandler(void) {

  /**********************************************************/
	/*					 Feedback System Value		  		  */
	/**********************************************************/

	if(ADC_GetITStatus(ADC1,ADC_IT_EOC) == SET)	{
		ADC_ClearITPendingBit(ADC1,ADC_IT_EOC);	
		
		converted_data_recvd_adc1 = ADC_GetConversionValue(ADC1);	
		
		/* ADC channel configures as OHT pump reads */
		if(adc_channel_type == ADC_Channel_0) {
			adc1_counter_OHT_tank++;
			converted_data_OHT_adc1 += converted_data_recvd_adc1;
		    if (adc1_counter_OHT_tank >= 100) {
				ADC_ITConfig (ADC1, ADC_IT_EOC, DISABLE);
				feedback_converted_avg_val = (converted_data_OHT_adc1 / 100);				
				feedback_cnvrtd_voltage = feedback_converted_avg_val * 3300 / 4095;		  				
				milli_volts_at_feedback = (feedback_cnvrtd_voltage % 1000) / 200;		
				converted_data_recvd_adc1 = 0;
				converted_data_OHT_adc1 = 0;			
				adc1_counter_OHT_tank = 0;
		
				oht_feedback_flag = 1;
		
				milli_volts_at_feedback = milli_volts_at_feedback > 0 ? 1: 0;
		//		if(oht_feedback_volt != milli_volts_at_feedback){
					oht_feedback_volt = milli_volts_at_feedback; 
					adc_evt_num |= OHT_FEEDBACK_CHNL;
					adc_monitoring = 1;
		//		}				
			}
		}

		
	
		/*     ADC HARDWARE VERSION FEEDBACK     */
		if(adc_channel_type == ADC_Channel_1){
			adc1_counter_version++;
			converted_data_Version_adc1 += converted_data_recvd_adc1;
			if(adc1_counter_version >= 20) {
				ADC_ITConfig (ADC1, ADC_IT_EOC, DISABLE);
				averaged_adc1_value = (converted_data_Version_adc1 / 20);	
				averaged_adc1_value = ((averaged_adc1_value * 3300) / 4095);		
	
				quetient = averaged_adc1_value/250;
				remaindr = averaged_adc1_value % 250;
					
				HwVersion[0] = 0;
				HwVersion[1] = 0;
				
				if(remaindr == 0) {
					HwVersion[2] = quetient;
				} else {
					HwVersion[2] = quetient + 1;
				}
				
				converted_data_recvd_adc1 = 0;
				converted_data_Version_adc1 = 0;			
				adc1_counter_version = 0;
				version_feedback_flag = 1;
			}
		}
  }
}
  
		
	


/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/


