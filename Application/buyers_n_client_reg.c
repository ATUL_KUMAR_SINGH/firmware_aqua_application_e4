/************************************************************************//**
* @file			buyers_n_client_reg.c
*
* @brief		This module contains the code for performing Buyer's registration & Client Registration process.
*
* @attention	Copyright Neotech Systems Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Neotech. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Neotech Management.
*
* @brief		First written on \em 14/12/2016 \n by \em Parvesh Kumar
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			This module contains the code for performing Buyer's registration & Client Registration process.
*				
****************************************************************************/
#include <stdint.h>
#include "buyers_n_client_reg.h"
#include "target.h"
//#include "vdp.h"
//#include "ethernet_packet.h"
#include "system.h"
#include "json_client.h"
#include "memory_map.h"
#include "client_registration.h"
#include "eeprom.h"
#include "schedule.h"



extern struct license_info licnse_info;
extern struct hc_packet gui_send;
extern struct sys_info system_info;
//extern struct system_config systm_info;

extern uint8_t license_class,license_type;
extern uint8_t mac_id_arr[12];
extern uint8_t mac_adr[6];

uint8_t gui_send_data[2];

extern uint8_t total_client_license;



//===========================================================================================================================
/*
	Func. Name : 	uint8_t device_registration(struct hc_payload_struct *hc_payload)

	Description :	This function performs the GUI Client Registration process.

  	@arg1 		:	struct hc_payload_struct *hc_payload
				
	@return :	Success / Failure status 
	@author : Parvesh
	@Date: 	 14/12/2016
*/
//===========================================================================================================================



uint8_t device_registration(struct json_struct *hc_payload)
{
	uint8_t temp_arr[200], read_arr[100];
	uint8_t retval;
	uint8_t temp;
	uint8_t no_of_GUI_clients_registered = 0, name_already_exists_flag = 0;


	memcpy(&temp_arr[0], (uint8_t *)&hc_payload->data[0], DEVICE_REG_LEN + 8);		// Copy the packet data in temp_arr[]

	switch(hc_payload->data[1])
	{
		case '2' : 				// HC(Black Box) is new

				temp_arr[0] = '1';	// The registration status has to be written as '1' for this GUI client .
				eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR, WRITE_OP ,&temp_arr[0], 1);					
		    eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + 1), WRITE_OP ,&temp_arr[4], 16);
		    eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + 17), WRITE_OP ,&temp_arr[22], 30);
		    eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + 47), WRITE_OP ,&temp_arr[54], 10);
				
		    //eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR, READ_OP ,&read_arr[0], DEVICE_REG_LEN);	// for testing purpose only
		
				system_info.status = 'Y';
				eeprom_data_read_write(SYSTEM_INFO_ADDR, WRITE_OP ,(uint8_t *)&system_info.status, 1);  // HC is now "not new" after writing 'Y' to this location.
		    //eeprom_data_read_write(SYSTEM_INFO_ADDR, READ_OP ,&read_arr[0], 1);											// for testing purpose only

				no_of_GUI_clients_registered++;	

				retval = '0';
				
				break;

		case '1' :				// HC(Black Box) is not new
				
				for(temp = 0; temp < total_client_license; temp++)
				{
					eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + (temp * DEVICE_REG_LEN)),READ_OP ,read_arr, DEVICE_REG_LEN);	  // status +mac id	
					if(read_arr[0] == '1')
					{
						no_of_GUI_clients_registered++;									
					}
				}

				if(no_of_GUI_clients_registered == total_client_license)
				{
				  retval = '1';	 	// No more GUI clients can be registered on currently available license.
				}
				else				// GUI client can be registered.
				{
					for(temp = 0; temp < total_client_license; temp++)
					{
						eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + (temp * DEVICE_REG_LEN)),READ_OP ,read_arr, DEVICE_REG_LEN);	  // status +mac id	
						if(read_arr[0] == '1')
						{
							if(strncmp(&temp_arr[22], &read_arr[17], NAME_LEN) == 0) // If both names match, then send failure.	
							{
							 	retval = '2';  // The name provided by GUI in the packet is already present in the list of registered GUI clients.
								name_already_exists_flag = 1;
							 	break;
							}
						}
					}
				   ////
				   if(name_already_exists_flag == 0)
				   {
				   		for(temp = 0; temp < total_client_license; temp++)
							{
								eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + (temp * DEVICE_REG_LEN)),READ_OP ,read_arr, DEVICE_REG_LEN);	  // status +mac id	
								if(read_arr[0] != '1')
								{
									temp_arr[0] = '1';
									//eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + (temp * DEVICE_REG_LEN)),WRITE_OP ,&temp_arr[0], DEVICE_REG_LEN);	
									
									eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + (temp * DEVICE_REG_LEN)), WRITE_OP ,&temp_arr[0], 1);					
									eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + (temp * DEVICE_REG_LEN) + 1), WRITE_OP ,&temp_arr[4], 16);
									eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + (temp * DEVICE_REG_LEN) + 17), WRITE_OP ,&temp_arr[22], 30);
									eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + (temp * DEVICE_REG_LEN) + 47), WRITE_OP ,&temp_arr[54], 10);

									retval = '0';
									break;					
								}
							}
					   ////
				   }
				}

				break;


		  default :  
		  		break;
	}	// closing of switch case
   /////	
	
	gui_send_data[0] = retval;
	return (retval);
	
}



//===========================================================================================================================
/*
	Func. Name : 	uint8_t Buyers_Registration_process(struct hc_payload_struct *hc_payload)

	Description :	This function performs the Buyers Registration process.

  	@arg1 		:	struct hc_payload_struct *hc_payload
				
	@return :	Success / Failure status 
	@author : Parvesh
	@Date: 	 13/12/2016
	
	NOTE : DEPENDENCY : "mac_adr[6]" is a global array & it must be filled with mac id value during "target_init" by reading value from dflash 
						 before using here.
					--->> systm_info.serial_num[0] & licnse_info.license_num[0] must also be read from dflash during target_init.
*/
//===========================================================================================================================
uint8_t Buyers_Registration_process(struct json_struct *hc_payload)
{
	uint8_t retval = '1';
	uint8_t temp = 0, count, loc, status, user_reg_val;
	uint8_t temp_arr[200];
	uint8_t read_arr[200];
	
		
	switch(hc_payload->data[1])					
	{
//--------------------------------------------------------------------------------------------------------------------------------------------------------------
		case '1':						// Buyer's Registration screen-1

			if(strncmp((char *)&system_info.serial_num[0], (char *)&hc_payload->data[4], SERIAL_NUM_LEN) == 0)	    // If HC Serial no. matches with the Serial no. that has arrived in the packet
			{
				for(count = 0; count < 6; count++, temp += 2){
					system_info.mac_id[temp] =	 ((mac_adr[count] & 0xf0) >> 4) + 0x30;
					system_info.mac_id[temp + 1] =	 (mac_adr[count] & 0x0f) + 0x30;
				}
				for(temp = 0; temp < 12; temp++){
					if(system_info.mac_id[temp] > 0x39){
					system_info.mac_id[temp] += 7;
					}
				}
		
				if(check_licencse_authenticate_logic((char *)&hc_payload->data[18], (char *)&system_info.serial_num[0], system_info.mac_id) == 1) // If License is valid
				{
					if(license_type == 1) // If License is of GUI type.
					{
						if(strncmp((uint8_t *)&system_info.license_num[0], (char *)&hc_payload->data[18], LICENSE_NUM_LEN) == 0)  // If License provided through GUI matches with the license no. stored on HC.
						retval = '0';
						 
						else
						 retval = '2';
					}
				   else
			   		retval = '2';
				}
			   else
			   	retval = '2';
			   
			}
		   else
	   		retval = '1';
		   

		   gui_send_data[0] = '1';	   // indicates that it is Buyer's Registration Screen-1
		   gui_send_data[1] = retval; // return value : '0'/'1'/'2'

		   break;
//--------------------------------------------------------------------------------------------------------------------------------------------------------------
			 
	  case '2' :							// Buyer's Registration screen-2

	  	loc = 1;
			//memcpy(&temp_arr[1], (char *)&hc_payload->data[1], (ADMIN_USER_PWD_MAXLEN + STD_USER_PWD_MAXLEN + BUYERS_NAME_LEN + BUYERS_CONTACT_NO_LEN + BUYERS_E_MAIL_ID_LEN));	 			
		   memcpy(&temp_arr[0], (char *)&hc_payload->data[0], (ADMIN_USER_PWD_MAXLEN + STD_USER_PWD_MAXLEN + BUYERS_NAME_LEN + BUYERS_CONTACT_NO_LEN + BUYERS_E_MAIL_ID_LEN + 13));
			
			status = chk_pwd_len(ADMIN_USER_TYPE, &temp_arr[4]);	// Check admin user pwd length

			if(status == '0')  // If (length of admin user pwd > 6 chars)
			{
				//status = chk_pwd_len(STD_USER_TYPE, &temp_arr[13]);

				//if(status == '0') // If (length of std user user pwd > 6 chars)
				//{ 
					//---------------------ADMIN_USER_PWD_MAXLEN-----------------------------------------------------------------------------------------------------------------------------------------
						// Save Buyer's Name, Contact no., E mail ID.
						eeprom_data_read_write(BUYERS_NAME_ADDR, WRITE_OP, (char *)&temp_arr[8 + (ADMIN_USER_PWD_MAXLEN + STD_USER_PWD_MAXLEN)],BUYERS_NAME_LEN); 		// Save Buyer's Name
				   // eeprom_data_read_write(BUYERS_NAME_ADDR, READ_OP, &read_arr[0],BUYERS_NAME_LEN); 		// Save Buyer's Name
						
						eeprom_data_read_write(BUYERS_CONTACT_NO_ADDR, WRITE_OP, (char *)&temp_arr[10 + (ADMIN_USER_PWD_MAXLEN + STD_USER_PWD_MAXLEN + BUYERS_NAME_LEN)], BUYERS_CONTACT_NO_LEN); 	// Save Buyer's Contact no.
					//	eeprom_data_read_write(BUYERS_CONTACT_NO_ADDR, READ_OP, &read_arr[0],BUYERS_CONTACT_NO_LEN);
				
						eeprom_data_read_write(BUYERS_E_MAIL_ID_ADDR, WRITE_OP, (char *)&temp_arr[12 + (ADMIN_USER_PWD_MAXLEN + STD_USER_PWD_MAXLEN + BUYERS_NAME_LEN + BUYERS_CONTACT_NO_LEN)], BUYERS_E_MAIL_ID_LEN);  // Save Buyer's E mail Id
					//	eeprom_data_read_write(BUYERS_E_MAIL_ID_ADDR, READ_OP, &read_arr[0],BUYERS_E_MAIL_ID_LEN);
					//--------------------------------------------------------------------------------------------------------------------------------------------------------------	
						// Save registration status = 1' for admin user & for standard user
						user_reg_val = '1';
						eeprom_data_read_write(DIFF_USER_TYPE_START_ADDR(ADMIN_USER_TYPE), WRITE_OP, &user_reg_val,1);	// Save registration status = 1' for admin user
						//eeprom_data_read_write(DIFF_USER_TYPE_START_ADDR(STD_USER_TYPE), WRITE_OP, &user_reg_val,1);		// Save registration status = 1' for standard user
						//eeprom_data_read_write(DIFF_USER_TYPE_START_ADDR(ADMIN_USER_TYPE), READ_OP, &read_arr[0],1);
					//--------------------------------------------------------------------------------------------------------------------------------------------------------------	
						// Save admin user password & standard user password
						eeprom_data_read_write((DIFF_USER_TYPE_START_ADDR(ADMIN_USER_TYPE) + 11), WRITE_OP, &temp_arr[4], ADMIN_USER_PWD_MAXLEN);		// Save admin user pwd
						//eeprom_data_read_write((DIFF_USER_TYPE_START_ADDR(STD_USER_TYPE) + 11), WRITE_OP, &temp_arr[1 + ADMIN_USER_PWD_MAXLEN], STD_USER_PWD_MAXLEN);			// Save standard user pwd
						//eeprom_data_read_write((DIFF_USER_TYPE_START_ADDR(ADMIN_USER_TYPE) + 11), READ_OP, &read_arr[0],ADMIN_USER_PWD_MAXLEN);
					//--------------------------------------------------------------------------------------------------------------------------------------------------------------
					system_info.status = '3'; // Indicates that Buyer's registration is done till now.
				  eeprom_data_read_write(SYSTEM_INFO_ADDR, WRITE_OP ,(uint8_t *)&system_info.status, 1);  
					
				 	retval = '0';
				//}
				//else
				 //retval = '4';	  // std user pwd length < 6 chars (failure)
			}
		   else
	   		retval = '3';  // admin user pwd length < 6 chars (failure)
	

		   gui_send_data[0] = '2';	   // indicates that it is Buyer's Registration Screen-2
		   gui_send_data[1] = retval;  // return value : '0'/'3'/'4'
			break;


	 }	// closing of switch case  
	////

	
	
	  return(retval);

}

//===========================================================================================================================
/*
	Func. Name : 	uint8_t chk_pwd_len(uint8_t user_type, uint8_t *buffptr)

	Description :	This function checks for the length of entered passwords : (admin pwd / std user pwd)
					If admin user pwd < 6 chars ---->> return failure	
					If std user pwd < 6 chars ---->> return failure	

  	@arg1 		:	uint8_t user_type
					This parameter can be :   ADMIN_USER_TYPE
											  STD_USER_TYPE	


	@arg2 		:	uint8_t *buffptr
				
	@return :	Success / Failure status 
	@author : Parvesh
	@Date: 	 13/12/2016
*/
//===========================================================================================================================
uint8_t chk_pwd_len(uint8_t user_type, uint8_t *buffptr)
{
	uint8_t retval;
	uint8_t i, space_charcnt = 0;

	if(user_type == ADMIN_USER_TYPE)
	{
		for(i = 0;i < ADMIN_USER_PWD_MAXLEN; i++)
		{
			if(*buffptr == ' ')				
			 space_charcnt++;
			
			buffptr++;
		}

		if(space_charcnt > 6)
		 retval = '3'; 	// admin user pwd < 6 chars in length (return failure)
		else
		 retval = '0';	// admin user pwd < 6 chars in length (return success)	
		 
				
	}
	else if(user_type == STD_USER_TYPE)
	{
		for(i = 0;i < STD_USER_PWD_MAXLEN; i++)
		{
			if(*buffptr == ' ')				
			 space_charcnt++;

			buffptr++;
		}

		if(space_charcnt > 6)
		 retval = '4'; 	// std user pwd < 6 chars in length (return failure)
		else
		 retval = '0';	// std user pwd < 6 chars in length (return success)	
	}
	
	return(retval);
} 


void clear_all_registered_clients(void)
{
	uint8_t temp, i;
	uint8_t temp_data = 0xFF;

	for(temp = 0; temp < 3; temp++)
	{
		for(i = 0; i < DEVICE_REG_LEN; i++)
		 eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + (temp * DEVICE_REG_LEN) + i), WRITE_OP ,&temp_data, 1);
	}
	
}






