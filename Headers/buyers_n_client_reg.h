/************************************************************************//**
* @file			"buyers_n_client_reg.h"
*
* @brief		Header for buyers_n_client_reg.c
*
* @attention	Copyright Neotech Systems Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Neotech. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Neotech Management.
*
* @brief		First written on \em 14/12/2016 \n by \em Parvesh Kumar
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
//#include "ethernet_packet.h"
#include <stdint.h>
#include "json_client.h"



#define 	ADMIN_USER_PWD_MAXLEN		12
#define 	STD_USER_PWD_MAXLEN			12
#define 	BUYERS_NAME_LEN					30
#define 	BUYERS_CONTACT_NO_LEN		10
#define 	BUYERS_E_MAIL_ID_LEN		40

#define 	NAME_LEN								30

#define 	ADMIN_USER_TYPE					1
#define 	STD_USER_TYPE						2

#ifndef __BUYERS_N_CLIENT_REG_H
#define __BUYERS_N_CLIENT_REG_H

uint8_t device_registration(struct json_struct *hc_payload);
uint8_t Buyers_Registration_process(struct json_struct *hc_payload);
uint8_t chk_pwd_len(uint8_t user_type, uint8_t *buffptr);
void clear_all_registered_clients(void);



#endif








