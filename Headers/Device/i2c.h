/**
  ******************************************************************************
  * @file    stm320518_eval.h
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    20-April-2012
  * @brief   This file contains definitions for STM320518_EVAL's Leds, push-buttons
  *          and COM ports hardware resources.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __I2C_H__
#define __I2C_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx.h"

typedef enum {
				I2C_IDLE,
				I2C_TRANSACT_START,
				I2C_MEM_ADD_SENT,
				I2C_TRANSACT_COMPLETE
}i2c_states;

typedef enum {
	I2C_WRITE,
	I2C_READ
}i2c_ops; 

extern i2c_states i2c_state;
extern i2c_ops i2c_op; 

extern uint8_t i2c_slave_addr;
extern uint8_t *i2c_mem_addr_ptr;
extern uint8_t *i2c_data_ptr;
extern uint8_t i2c_num_of_bytes;

void i2c_init (void);

  
#ifdef __cplusplus
}
#endif

#endif /* __STM320518_EVAL_H */
/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */  

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
