#include "stdint.h"

#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#define MODE_AUTOMATIC			0
#define MODE_MANUAL				1

struct __attribute__((__packed__)) system_params {
	uint8_t sampling_window					:4;			/* 0 - 15 seconds. */
	uint8_t check_di_debounce				:4;			/* Input Debounce Bits. */
	uint8_t sys_mode						:1;			/* Selected System Mode. */
	uint8_t __1sFlag						:1;
	uint8_t __25msFlag						:1;
	uint8_t snd_reg_pkt_flg					:1;
	uint8_t brdcst_tkn_rcvd					:1;
	/* Self Slave Address. */
	uint8_t slave_add;
};

struct __attribute__((__packed__)) led_params {
//	uint8_t comm_led_100ms_iter				:4;			/* 0 - 15 seconds. */
	uint8_t comm_led_timeout				:4;			/* Comm Led Timeout. */
	uint8_t sys_led_100ms_iter				:4;			/* 0 - 15 seconds. */
	uint8_t sys_led_timeout					:4;			/* Comm Led Timeout. */
	uint8_t comm_led_state					:1;
	uint8_t sys_led_state					:1;
	uint8_t __100msFlag						:1;
};

void init_sys_params (void);
void init_led_params (void);
void pre_init (void);
void init_pir_subsystem (void);
void serve_led_timer (void);
void init_system_mode (void);
void process_pir_activity_time_pkt (uint8_t *settings_data_ptr);
//------------------------------------------------------------------------------------
										// BY PARVESH
void toggle_comm_led(uint16_t times);
void Delay(uint32_t del);
//------------------------------------------------------------------------------------
#endif
