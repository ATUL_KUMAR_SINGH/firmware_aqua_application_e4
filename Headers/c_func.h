/************************************************************************//**
* @file			c_func.h
*
* @brief		Header for c_func.c
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 04/05/14 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
/*
**===========================================================================
**		Include section
**===========================================================================
*/
#ifndef C_FUNC_H
#define C_FUNC_H

long ascii_dec(uint8_t nob);
void dec_ascii_byte(uint32_t long_value, uint8_t *convrt_ptr, uint8_t nob);
long byte_integer_conversion(uint8_t *convrt_ptr, uint8_t nob);
void integer_byte_conversion(uint32_t long_value, uint8_t *convrt_ptr, uint8_t nob);
long ascii_decimal(uint8_t *convrt_ptr, uint8_t nob);
#endif
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
