/************************************************************************//**
* @file			target.h
*
* @brief		Header for target.c
*
* @attention	Copyright 2014 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 27/012/11 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/

#ifndef TARGET_H
#define TARGET_H



/*
**===========================================================================
**		Modules define section
**===========================================================================
*/

#define LED_MODULE_ENABLE

#define EEPROM_MODULE_ENABLE

#define USB_MODULE_ENABLE

#define WIFI_MODULE_ENABLE

#define FEEDBACK_MODULE_ENABLE

#define RTC_MODULE_ENABLE

#define	B3_PROJ

#define FLAG_WRITE_VER_GREATER_THAN_0_1_27

//#define	E2_PROJ

//#define	E4_PROJ

#endif